package cas.common;

import java.util.function.Consumer;

import cas.detail.Environment;
import dave.util.Identifiable;

public interface Generator extends Identifiable, Consumer<Environment>
{
}
