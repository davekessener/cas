package cas.common;

import cas.detail.Environment;
import dave.util.Identifiable;

public interface Rule extends Identifiable
{
	public default boolean applicable(Environment e) { return true; }
}
