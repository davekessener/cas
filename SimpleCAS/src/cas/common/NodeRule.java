package cas.common;

import cas.detail.Agent;
import cas.detail.Environment;

public interface NodeRule extends Rule
{
	public abstract void apply(Environment e, Agent a);
}
