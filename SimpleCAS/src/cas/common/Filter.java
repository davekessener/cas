package cas.common;

import java.util.stream.Stream;

import cas.detail.Environment;

public interface Filter
{
	public abstract boolean test(Environment e);
	
	public static class Constant implements Filter
	{
		private final double mChance;
		
		public Constant( ) { this(1.0); }
		public Constant(double c) { mChance = Math.max(0, Math.min(c, 1)); }
		
		@Override
		public boolean test(Environment e)
		{
			return (mChance > 0) && (mChance >= 1.0 || e.random().nextDouble() < mChance);
		}
	}

	public static class And implements Filter
	{
		private final Filter[] mF;
		
		public And(Filter ... f)
		{
			mF = f;
		}
		
		@Override
		public boolean test(Environment e)
		{
			return !Stream.of(mF).filter(f -> !f.test(e)).findAny().isPresent();
		}
	}

	public static class Or implements Filter
	{
		private final Filter[] mF;
		
		public Or(Filter ... f)
		{
			mF = f;
		}
		
		@Override
		public boolean test(Environment e)
		{
			return Stream.of(mF).filter(f -> f.test(e)).findAny().isPresent();
		}
	}
}
