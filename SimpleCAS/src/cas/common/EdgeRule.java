package cas.common;

import cas.detail.Environment;
import cas.detail.Relation;

public interface EdgeRule extends Rule
{
	public abstract void apply(Environment e, Relation r);
}
