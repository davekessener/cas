package cas.gen;

import java.util.stream.IntStream;

import cas.detail.Agent;
import cas.detail.Environment;
import dave.json.Container;
import dave.json.JSON;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;

@Container
public class UniformGenerator extends BaseGenerator implements Saveable
{
	public static interface Initializer { void initialize(Environment e, Agent a, int x, int y); }
	
	private final int mWidth, mHeight;
	private final Initializer mCallback;
	
	public UniformGenerator(int w, int h, Initializer f)
	{
		super("uniform");
		
		mWidth = w;
		mHeight = h;
		mCallback = f;
	}

	@Override
	public void accept(Environment e)
	{
		Agent[] a = new Agent[mWidth * mHeight];
		
		for(int y = 0 ; y < mHeight ; ++y)
		{
			for(int x = 0 ; x < mWidth ; ++x)
			{
				int i = toID(x, y);
				
				a[i] = e.create();
				
				mCallback.initialize(e, a[i], x, y);
			}
		}
		
		for(int y = 0 ; y < mHeight ; ++y)
		{
			for(int x = 0 ; x < mWidth ; ++x)
			{
				int i = toID(x, y);
				
				e.connect(a[i], a[toID(x + 1, y)]);
				e.connect(a[i], a[toID(x, y + 1)]);
			}
		}
	}
	
	@Override
	@Saver
	public JsonValue save( )
	{
		JsonObject json = new JsonObject();
		
		json.putInt("width", mWidth);
		json.putInt("height", mHeight);
		json.put("initializer", JSON.serialize(mCallback));
		
		return json;
	}
	
	@Loader
	public static UniformGenerator load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		int w = o.getInt("width");
		int h = o.getInt("height");
		Initializer cb = (Initializer) JSON.deserialize(o.get("initializer"));
		
		return new UniformGenerator(w, h, cb);
	}
	
	private int toID(int x, int y) { return ((x + mWidth) % mWidth) + mWidth * ((y + mHeight) % mHeight); }
	
	@Container
	public static class RandomInitializer implements Initializer, Saveable
	{
		private final double mChance, mFactor;
		
		public RandomInitializer(double c, double f)
		{
			mChance = c;
			mFactor = f;
		}
		
		@Override
		public void initialize(Environment e, Agent a, int x, int y)
		{
			IntStream.range(0, e.size())
				.filter(i -> e.random().nextDouble() < mChance)
				.forEach(i -> a.set(i, (e.random().nextDouble() - 0.5) * 2 * mFactor));
		}
		
		@Override
		@Saver
		public JsonValue save( )
		{
			JsonObject json = new JsonObject();
			
			json.putNumber("chance", mChance);
			json.putNumber("factor", mFactor);
			
			return json;
		}
		
		@Loader
		public static RandomInitializer load(JsonValue json)
		{
			JsonObject o = (JsonObject) json;
			
			double c = o.getDouble("chance");
			double f = o.getDouble("factor");
			
			return new RandomInitializer(c, f);
		}
	}
}
