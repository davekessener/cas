package cas.gen;

import cas.detail.Agent;
import cas.detail.Environment;
import dave.json.Container;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;

@Container
public class TriangleGenerator extends BaseGenerator implements Saveable
{
	private final int mCount;
	private final double mRetention, mScale, mTurnover;
	
	public TriangleGenerator(int n, double r, double s, double t)
	{
		super("triangle");
		
		mCount = n;
		mRetention = r;
		mScale = s;
		mTurnover = t;
	}

	@Override
	public void accept(Environment e)
	{
		final Agent[] cores = new Agent[] { e.create(), e.create(), e.create() };
		
		for(int i = 0, o = (int) Math.floor(mTurnover * e.size()) ; i < e.size() ; ++i)
		{
			cores[0].set(i, (2 * e.random().nextDouble() - 1) * mScale);
			
			double v = cores[0].get(i) * (i < o ? 1 : -1);
			
			cores[1].set(i, v);
			cores[2].set(i, v);
		}

		e.connect(cores[0], cores[1]);
		e.connect(cores[0], cores[2]);
		
		cores[0].tag("COLOR:#0000FF");
		cores[1].tag("COLOR:#FF0000");
		cores[2].tag("COLOR:#FF0000");
		
		for(int k = 0 ; k < 2 ; ++k)
		{
			for(int j = 0 ; j < mCount ; ++j)
			{
				Agent a = e.create();
				
				for(int i = 0 ; i < e.size() ; ++i)
				{
					if(e.random().nextDouble() < mRetention)
					{
						a.set(i, cores[k].get(i));
					}
				}
				
				e.connect(a, cores[k]);
				e.connect(a, cores[2 * k]);
				
				a.tag("COLOR:#" + (k == 0 ? "000080" : "800000"));
			}
		}
	}
	
	@Saver
	@Override
	public JsonValue save( )
	{
		JsonObject json = new JsonObject();
		
		json.putInt("count", mCount);
		json.putNumber("retention", mRetention);
		json.putNumber("scale", mScale);
		json.putNumber("turnover", mTurnover);
		
		return json;
	}
	
	@Loader
	public static TriangleGenerator load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		int count = o.getInt("count");
		double retention = o.getDouble("retention");
		double scale = o.getDouble("scale");
		double turnover = o.getDouble("turnover");
		
		return new TriangleGenerator(count, retention, scale, turnover);
	}
}
