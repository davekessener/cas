package cas.gen;

import cas.common.Generator;
import cas.detail.Agent;
import dave.util.RNG;

public abstract class BaseGenerator implements Generator
{
	private final String mID;
	
	protected BaseGenerator(String id)
	{
		mID = id;
	}
	
	@Override
	public String getID( )
	{
		return mID;
	}
	
	protected static void initialize(Agent a, RNG rng, double c, double s)
	{
		for(int i = 0 ; i < a.size() ; ++i)
		{
			a.set(i, (rng.nextDouble() * 2 - 1) * s);
		}
	}
}
