package cas.gen;

import cas.detail.Agent;
import cas.detail.Environment;
import cas.util.UniformRange;
import dave.json.Container;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;

@Container
public class RandomGenerator extends BaseGenerator implements Saveable
{
	private final int mNodes, mEdges;
	private final double mChance, mScale;
	
	public RandomGenerator(int m, int k, double c, double s)
	{
		super("random-generator");
		
		mNodes = m;
		mEdges = k;
		mChance = c;
		mScale = s;
	}

	@Override
	public void accept(Environment e)
	{
		Agent[] agents = new Agent[mNodes];
		
		for(int i = 0 ; i < mNodes ; ++i)
		{
			agents[i] = e.create();
			
			BaseGenerator.initialize(agents[i], e.random(), mChance, mScale);
			
			for(int j = i - 1 ; j >= 0 ; --j)
			{
				if(j == 0 || e.random().nextDouble() < 0.5)
				{
					e.connect(agents[i], agents[j]);
					
					break;
				}
			}
		}
		
		UniformRange rng = new UniformRange(0, mNodes, e.random());
		for(int k = mEdges ; k > 0 ;)
		{
			int ai = (int) rng.next();
			int bi = (int) rng.next();
			
			if(ai != bi && !e.relations().anyMatch(r -> r.includes(ai) && r.includes(bi)))
			{
				e.connect(ai, bi);
				
				--k;
			}
		}
	}
	
	@Override
	@Saver
	public JsonValue save( )
	{
		JsonObject json = new JsonObject();
		
		json.putInt("nodes", mNodes);
		json.putInt("edges", mEdges);
		json.putNumber("chance", mChance);
		json.putNumber("scale", mScale);
		
		return json;
	}
	
	@Loader
	public static RandomGenerator load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		int m = o.getInt("nodes");
		int k = o.getInt("edges");
		double c = o.getDouble("chance");
		double s = o.getDouble("scale");
		
		return new RandomGenerator(m, k, c, s);
	}
}
