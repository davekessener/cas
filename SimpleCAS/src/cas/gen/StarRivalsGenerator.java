package cas.gen;

import cas.detail.Agent;
import cas.detail.Environment;
import dave.json.Container;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saver;

@Container
public class StarRivalsGenerator extends BaseGenerator
{
	private final int mCore;
	private final int mClusterCount;
	private final double mPerivRetention;
	private final double mScale;
	private final double mCross;
	
	public StarRivalsGenerator(int c, int n, double f, double s, double x)
	{
		super("star-shaped-rivals");
		
		mCore = c;
		mClusterCount = n;
		mPerivRetention = f;
		mScale = s;
		mCross = x;
	}

	@Override
	public void accept(Environment e)
	{
		Agent[] a = { e.create(), e.create() };
		
		for(int i = 0 ; i < e.size() ; ++i)
		{
			a[0].set(i, mScale * (e.random().nextDouble() - 0.5));
			a[1].set(i, -a[0].get(i));
		}
		
		a[0].tag("core_0").tag("COLOR:#FF0000");
		a[1].tag("core_1").tag("COLOR:#0000FF");
		
		Agent[][] b = new Agent[2][mCore];
		
		if(mCore > 0)
		{
			for(int i = 0 ; i < 2 ; ++i)
			{
				for(int j = 0 ; j < mCore ; ++j)
				{
					b[i][j] = e.create(a[i], mPerivRetention * 2);
					
					for(int k = 0 ; k < mClusterCount ; ++k)
					{
						Agent c = e.create(a[i], mPerivRetention);
						
						c.tag("periv_" + i).tag("COLOR:#" + (i == 0 ? "FFFF00" : "00FFFF"));
						
						e.connect(b[i][j], c);
					}
					
					b[i][j].tag("cluster_" + i).tag("COLOR:#" + (i == 0 ? "800000" : "000080"));
					
					e.connect(a[i], b[i][j]);
				}
				
				for(int j = 0 ; j < mCore ; ++j)
				{
					e.connect(b[i][j], b[i][(j + 1) % mCore]);
				}
			}
		}
		else
		{
			for(int i = 0 ; i < 2 ; ++i)
			{
				for(int j = 0 ; j < mClusterCount ; ++j)
				{
					Agent c = e.create(a[i], mPerivRetention);
					
					c.tag("cluster_" + i).tag("COLOR:#" + (i == 0 ? "FFFF00" : "00FFFF"));
					
					e.connect(a[i], c);
				}
			}
		}
		
		if(mCross > 0)
		{
			for(int i = 0, c = (int) Math.ceil(mCross * mCore) ; i < c ; ++i)
			{
				e.connect(b[0][i], b[1][i]);
			}
		}
		else
		{
			e.connect(a[0], a[1]);
		}
	}
	
	@Saver
	public JsonValue save( )
	{
		JsonObject json = new JsonObject();
		
		json.putInt("core", mCore);
		json.putInt("edge", mClusterCount);
		json.putNumber("retention", mPerivRetention);
		json.putNumber("scale", mScale);
		json.putNumber("cross", mCross);
		
		return json;
	}
	
	@Loader
	public static StarRivalsGenerator load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		int core = o.getInt("core");
		int count = o.getInt("edge");
		double ret = o.getNumber("retention");
		double scale = o.getNumber("scale");
		double cross = o.getNumber("cross");
		
		return new StarRivalsGenerator(core, count, ret, scale, cross);
	}
}
