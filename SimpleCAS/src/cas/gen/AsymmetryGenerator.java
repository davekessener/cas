package cas.gen;

import cas.detail.Agent;
import cas.detail.Environment;
import dave.json.Container;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;

@Container
public class AsymmetryGenerator extends BaseGenerator implements Saveable
{
	private final int[] mCount;
	private final double mRetention, mFactor;
	
	public AsymmetryGenerator(int n0, int n1, double r, double f)
	{
		super("asymmetric-rivals");
		
		mCount = new int[] { n0, n1 };
		mRetention = r;
		mFactor = f;
	}

	@Override
	public void accept(Environment e)
	{
		Agent[] a = new Agent[] { e.create(), e.create() };

		a[0].tag("center_0");
		a[1].tag("center_1");
		
		for(int i = 0 ; i < e.size() ; ++i)
		{
			a[0].set(i, (2 * e.random().nextDouble() - 1) * mFactor);
			a[1].set(i, -a[0].get(i));
		}
		
		for(int k = 0 ; k < 2 ; ++k)
		{
			for(int j = 0 ; j < mCount[k] ; ++j)
			{
				Agent b = e.create();
				
				b.tag("cluster_" + k);
				
				for(int i = 0 ; i < e.size() ; ++i)
				{
					if(e.random().nextDouble() < mRetention)
					{
						b.set(i, a[k].get(i));
					}
				}
				
				e.connect(a[k], b);
			}
		}
		
		e.connect(a[0], a[1]);
	}
	
	@Override
	@Saver
	public JsonValue save( )
	{
		JsonObject json = new JsonObject();
		
		json.putInt("cluster_a", mCount[0]);
		json.putInt("cluster_b", mCount[1]);
		json.putNumber("retention", mRetention);
		json.putNumber("factor", mFactor);
		
		return json;
	}
	
	@Loader
	public static AsymmetryGenerator load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		int a = o.getInt("cluster_a");
		int b = o.getInt("cluster_b");
		double r = o.getDouble("retention");
		double f = o.getDouble("factor");
		
		return new AsymmetryGenerator(a, b, r, f);
	}
}
