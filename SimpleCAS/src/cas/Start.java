package cas;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.stream.Collectors;

import cas.detail.Configuration;
import cas.detail.Controller;
import cas.rules.Rule_211;
import cas.rules.Rule_212;
import cas.rules.Rule_213;
import cas.rules.Rule_222;
import cas.rules.Rule_223;
import cas.rules.Rule_224;
import cas.util.CASUtils;
import cas.work.CoreParam;
import cas.work.CorenessParam;
import cas.work.DBSCANClusterParam;
import cas.work.RivalsOWParam;
import cas.work.ValueAverageParam;
import dave.arguments.Arguments;
import dave.arguments.Option;
import dave.arguments.Option.OptionBuilder;
import dave.arguments.ParseException;
import dave.arguments.Parser;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.StringBuffer;
import dave.json.PrettyPrinter;
import dave.util.Seed;
import dave.util.ShutdownService;
import dave.util.ShutdownService.Priority;
import dave.util.config.OptionHash;
import dave.util.log.LogBase;
import dave.util.log.Logger;
import dave.util.log.Severity;

public class Start
{
	public static void main(String[] args) throws ParseException, IOException
	{
		LogBase.INSTANCE.registerSink(e -> true, new LogBase.DefaultSink(System.err));
		LogBase.INSTANCE.start();
		ShutdownService.INSTANCE.register(Priority.LAST, LogBase.INSTANCE::stop);
		
		Logger.DEFAULT.log(Severity.INFO, "Complex Adaptive Systems v%d", Configuration.VERSION.getValue());
		
		try
		{
			run(args);
		}
		catch(Throwable e)
		{
			e.printStackTrace();
			
			System.err.println("usage: ./cas [--config=config_file.json] [--initial=initial_state.json] [--output=out_file_path.json]");
		}
		
		ShutdownService.INSTANCE.shutdown();
	}
	
	private static void run(String[] args) throws ParseException, IOException
	{
		dave.util.config.Configuration<Params> config = parseArgs(args);
		
		Controller ctrl = null;
		Seed seed = (Seed) config.get(Params.SEED);
		JsonValue initial = (JsonValue) config.get(Params.INITIAL);
		JsonValue cfg = (JsonValue) config.get(Params.CONFIG);
		File out = (File) config.get(Params.OUTFILE);
		File logfile = (File) config.get(Params.LOGFILE);
		
		try(FileOutputStream log = new FileOutputStream(logfile))
		{
			if(cfg != null)
			{
				Configuration.load(cfg);
			}
	
			if(initial != null)
			{
				ctrl = Controller.load(initial);
			}
			else
			{
				ctrl = new Controller(Configuration.INTERESTS.getValue());
				
				ctrl.add(new Rule_211())
					.add(new Rule_212())
					.add(new Rule_213())
					.add(new Rule_222())
					.add(new Rule_223())
					.add(new Rule_224())
					;
			
				ctrl.generate(Configuration.get(Configuration.GENERATOR.getValue()));
				
				initial = ctrl.save();
			}
			
			if(seed != null)
			{
				ctrl.environment().random().setSeed(seed);
			}
			else
			{
				seed = ctrl.environment().random().getSeed();
			}
	
			ctrl.add(new CorenessParam());
			ctrl.add(new DBSCANClusterParam());
			ctrl.add(new ValueAverageParam());
			ctrl.add(new CoreParam());
			ctrl.add(new RivalsOWParam());
			
			ctrl.run(CASUtils.generateWatcher(log));
			
			JsonObject graph = new JsonObject();
			
			graph.putString("seed", seed.toString());
			graph.put("graph", ctrl.graph());
			graph.put("initial", initial);
			graph.put("config", Configuration.save());
			
			String result = graph.toString(new PrettyPrinter());
			
			if(out != null)
			{
				BufferedWriter o = new BufferedWriter(new FileWriter(out));
				
				o.write(result);
				o.close();
			}
			else
			{
				System.out.println(result);
			}
		}
		catch(IOException e)
		{
			throw e;
		}
	}
	
	private static dave.util.config.Configuration<Params> parseArgs(String[] args) throws ParseException, IOException
	{
		dave.util.config.Configuration<Params> config = new OptionHash<>();
		Option o_config = (new OptionBuilder("config")).setShortcut("c").hasValue(true).build();
		Option o_initial = (new OptionBuilder("initial")).setShortcut("i").hasValue(true).build();
		Option o_seed = (new OptionBuilder("seed")).setShortcut("s").hasValue(true).build();
		Option o_out = (new OptionBuilder("output")).setShortcut("o").hasValue(true).build();
		Option o_log = (new OptionBuilder("log")).setForced(true).hasValue(true).build();
		Parser p = new Parser(o_config, o_seed, o_out, o_initial, o_log);
		Arguments a = p.parse(args);
		
		config.set(Params.LOGFILE, new File(a.getArgument(o_log)));
		
		if(a.hasArgument(o_config))
		{
			config.set(Params.CONFIG, readJsonFile(a.getArgument(o_config)));
		}
		
		if(a.hasArgument(o_initial))
		{
			config.set(Params.INITIAL, readJsonFile(a.getArgument(o_initial)));
		}

		if(a.hasArgument(o_seed))
		{
			config.set(Params.SEED, new Seed(a.getArgument(o_seed)));
		}

		if(a.hasArgument(o_out))
		{
			config.set(Params.OUTFILE, new File(a.getArgument(o_out)));
		}
		
		return config;
	}
	
	private static JsonValue readJsonFile(String path) throws IOException
	{
		File f = new File(path);
		String raw = Files.readAllLines(f.toPath()).stream().collect(Collectors.joining(" "));
		
		return JsonValue.read(new StringBuffer(raw));
	}
	
	private static enum Params implements dave.util.config.Option
	{
		CONFIG(null),
		INITIAL(null),
		SEED(null),
		OUTFILE(null),
		LOGFILE(null);
		
		private Params(Object a)
		{
			this.mArg = a;
		}
		
		private final Object mArg;

		@Override
		public Object getDefault()
		{
			return mArg;
		}
	}
}
