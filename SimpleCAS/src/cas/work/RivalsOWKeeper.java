package cas.work;

public class RivalsOWKeeper extends BaseKeeper
{
	public RivalsOWKeeper( )
	{
		super("rivals-ow");
	}

	@Override
	public String process(Framework f)
	{
		Double ow = f.get("rivals-ow");
		
		return ow.isInfinite() ? null : String.format("%.4f", ow);
	}
}
