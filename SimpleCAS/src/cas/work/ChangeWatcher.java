package cas.work;

import java.util.Set;
import java.util.stream.Collectors;

import cas.detail.Environment;
import cas.detail.Relation;
import dave.json.JsonConstant;
import dave.json.JsonValue;
import dave.json.Loadable;
import dave.json.Saveable;

public class ChangeWatcher implements Watcher, Saveable, Loadable
{
	private Set<Relation> mRelations;
	
	@Override
	public boolean evaluate(Framework f)
	{
		Environment e = f.get("environment");
		Set<Relation> r = e.relations().collect(Collectors.toSet());
		
		boolean done = (mRelations != null && !r.equals(mRelations));
		
		mRelations = r;
		
		return !done;
	}

	@Override
	public void load(JsonValue json)
	{
	}

	@Override
	public JsonValue save()
	{
		return JsonConstant.NULL;
	}
}
