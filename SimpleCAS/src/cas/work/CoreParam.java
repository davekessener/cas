package cas.work;

import java.util.Arrays;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import cas.detail.Agent;
import cas.detail.Environment;

public class CoreParam extends BaseParameter
{
	public CoreParam( )
	{
		super("cores");
	}

	@Override
	public Object calculate(Framework fw)
	{
		Environment e = fw.get("environment");
		return e.agents()
			.filter(a -> Arrays.stream(a.tags()).anyMatch(IS_CORE))
			.sorted((a, b) -> GET_TAG.apply(a).compareTo(GET_TAG.apply(b)))
			.collect(Collectors.toList());
	}
	
	private static final Predicate<String> IS_CORE = s -> s.startsWith("core_");
	private static final Function<Agent, String> GET_TAG = a -> Arrays.stream(a.tags()).filter(IS_CORE).findAny().get();
}
