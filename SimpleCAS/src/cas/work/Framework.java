package cas.work;

public interface Framework
{
	public abstract <T> T get(String id);
}
