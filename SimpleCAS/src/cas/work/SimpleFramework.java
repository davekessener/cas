package cas.work;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class SimpleFramework<T> implements Framework
{
	private final Map<String, Parameter> mParams;
	private final Map<String, Object> mCache;
	private final String mID;
	private final T mSource;
	
	public SimpleFramework(String id, T o)
	{
		mID = id;
		mParams = new HashMap<>();
		mCache = new HashMap<>();
		mSource = o;
	}
	
	public void addParameter(Parameter p)
	{
		mParams.put(p.getID(), p);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <TT> TT get(String id)
	{
		Object r = null;
		
		if(id.equals(mID))
		{
			r = mSource;
		}
		else if(mCache.containsKey(id))
		{
			r = mCache.get(id);
		}
		else
		{
			mCache.put(id, r = mParams.get(id).calculate(this));
		}
		
		return (TT) r;
	}
	
	@SafeVarargs
	public static <T> Framework construct(String id, T src, List<Parameter> p, Parameter ... ps)
	{
		SimpleFramework<T> f = new SimpleFramework<>(id, src);
		
		p.forEach(f::addParameter);
		Stream.of(ps).forEach(f::addParameter);
		
		return f;
	}
}
