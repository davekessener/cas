package cas.work;

import java.util.Arrays;

import cas.detail.Environment;

public class ValueAverageParam extends BaseParameter
{
	public ValueAverageParam( )
	{
		super("val-avg");
	}

	@Override
	public Object calculate(Framework fw)
	{
		Environment e = fw.get("environment");
		long n = e.agents().count();
		double[] m = new double[e.size()];
		double[] d1 = new double[e.size()];
		double[] d2 = new double[e.size()];
		
		e.agents().forEach(a -> {
			for(int i = 0 ; i < m.length ; ++i)
			{
				m[i] += a.get(i);
			}
		});
		
		for(int i = 0 ; i < m.length ; ++i)
		{
			m[i] /= n;
		}
		
		e.agents().forEach(a -> {
			for(int i = 0 ; i < m.length ; ++i)
			{
				double v = a.get(i);
				double d = v - m[i];
				
				d1[i] += d * d;
				d2[i] += v * v;
			}
		});
		
		for(int i = 0 ; i < m.length ; ++i)
		{
			d1[i] = Math.sqrt(d1[i] / n);
			d2[i] = Math.sqrt(d2[i] / n);
		}
		
		double mm = Arrays.stream(m).average().getAsDouble();
		double md1 = Arrays.stream(d1).average().getAsDouble();
		double md2 = Arrays.stream(d2).average().getAsDouble();
		
		return new Result(m, mm, d1, md1, d2, md2);
	}
	
	public static final class Result
	{
		public final double[] means, deviations, neutrality;
		public final double mean_mean, mean_deviation, mean_neutral;
		
		private Result(double[] m, double mm, double[] d1, double md1, double[] d2, double md2)
		{
			means = m;
			deviations = d1;
			neutrality = d2;
			mean_mean = mm;
			mean_deviation = md1;
			mean_neutral = md2;
		}
	}
}
