package cas.work;

import java.util.List;
import java.util.Set;

import cas.detail.Environment;
import cas.util.Selector;
import dave.json.JsonConstant;
import dave.json.JsonValue;
import dave.json.Loadable;
import dave.json.Saveable;
import dave.util.log.Logger;
import dave.util.log.Severity;

public class SingularityWatcher implements Watcher, Saveable, Loadable
{
	private final int[] mCounter = { 0, 0 };
	
	@Override
	public boolean evaluate(Framework f)
	{
		Environment e = f.get("environment");
		List<Set<Integer>> coreness = f.get("coreness");
		
		Selector<Double> min = new Selector<>(Double.MAX_VALUE, (a, b) -> (a > b));
		
		e.relations().forEach(r -> min.add(e.orientation(r)));
		
		++mCounter[min.get() < 0 ? 0 : 1];
		
		boolean done = (coreness.get(0).size() == e.agents().count() - 1);
		
		if(done)
		{
			Integer step = f.get("step");
			
			Logger.DEFAULT.log(Severity.INFO, "PIVOT @%d (%.3f)", mCounter[0], mCounter[0] / (double) (mCounter[0] + mCounter[1]));
			Logger.DEFAULT.log(Severity.INFO, "TOTAL @%d in %d", mCounter[0] + mCounter[1], step);
		}
		
		return !done;
	}

	@Override
	public void load(JsonValue json)
	{
		mCounter[0] = mCounter[1] = 0;
	}

	@Override
	public JsonValue save()
	{
		return JsonConstant.NULL;
	}
}
