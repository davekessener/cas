package cas.work;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import dave.util.ImmutableMapBuilder;

public final class KeeperManager implements Consumer<Framework>
{
	private static final Map<String, Keeper> sKeepers = (new ImmutableMapBuilder<String, Keeper>())
		.put("average-mean", new SimpleKeeper<ValueAverageParam.Result>("average-mean", "val-avg", r -> String.format("%.3f", r.mean_mean)))
		.put("average-deviation", new SimpleKeeper<ValueAverageParam.Result>("average-deviation", "val-avg", r -> String.format("%.3f", r.mean_deviation)))
		.put("average-neutrality", new SimpleKeeper<ValueAverageParam.Result>("average-neutrality", "val-avg", r -> String.format("%.3f", r.mean_neutral)))
		.put("rivals-ow", new RivalsOWKeeper())
		.build();
	
	private static Keeper get(String id)
	{
		Keeper k = sKeepers.get(id);
		
		if(k == null)
			throw new IllegalArgumentException(id);
		
		return k;
	}
	
	public static interface Callback { void accept(Framework fw, String id, String v); }
	
	private final Callback mCallback;
	private final List<Keeper> mKeepers;
	private final Map<String, String> mCache;
	
	public KeeperManager(Consumer<String> f) { this(new DefaultCallback(f)); }
	public KeeperManager(Callback f)
	{
		mCallback = f;
		mKeepers = new ArrayList<>();
		mCache = new HashMap<>();
	}
	
	public KeeperManager add(String id)
	{
		mKeepers.add(get(id));
		
		return this;
	}
	
	@Override
	public void accept(Framework fw)
	{
		mKeepers.forEach(k -> {
			String r = k.process(fw);
			
			if(r != null)
			{
				String id = k.getID();
				String o = mCache.get(id);
				
				if(!r.equals(o))
				{
					mCache.put(id, r);
					mCallback.accept(fw, id, r);
				}
			}
		});
	}

	public static class DefaultCallback implements Callback
	{
		private final Consumer<String> mCallback;
		
		public DefaultCallback(Consumer<String> f)
		{
			mCallback = f;
		}
		
		@Override
		public void accept(Framework fw, String id, String v)
		{
			mCallback.accept(String.format("@%d: %s = %s", fw.get("step"), id, v));
		}
	}
}
