package cas.work;

import dave.util.log.Logger;

public class Supervisor implements Watcher
{
	private final int mMin, mMax;
	private final Watcher mSub;
    private long mLast;
	
	public Supervisor(int min, int max, Watcher s)
	{
		mMin = min;
		mMax = max;
		mSub = s;
        mLast = 0;
	}
	
	@Override
	public boolean evaluate(Framework f)
	{
		Integer n = f.get("step");
        long now = System.currentTimeMillis();

        if(now - mLast > MSG_INT)
        {
            mLast = now;

            Logger.DEFAULT.log("%.1f%%", n * 100.0 / mMax);
        }
		
		return (n < mMin) || (n < mMax && mSub.evaluate(f));
	}

    private static final int MSG_INT = 5000;
}
