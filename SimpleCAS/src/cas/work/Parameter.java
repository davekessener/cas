package cas.work;

import dave.util.Identifiable;

public interface Parameter extends Identifiable
{
	public abstract Object calculate(Framework fw);
}
