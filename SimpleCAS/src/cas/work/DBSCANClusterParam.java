package cas.work;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import cas.detail.Agent;
import cas.detail.Environment;

public class DBSCANClusterParam extends BaseParameter
{
	public DBSCANClusterParam( )
	{
		super("dbscan");
	}

	@Override
	public Object calculate(Framework fw)
	{
		Environment e = fw.get("environment");
		List<Set<Agent>> cluster = new ArrayList<>();
		Map<Agent, Double> weights = new HashMap<>();
		Queue<Agent> open = new ArrayDeque<>();
		
		e.agents().forEach(a -> {
			open.add(a);
			weights.put(a, 1 + e.value(a));
		});
		
		while(!open.isEmpty())
		{
			Agent a = open.poll();
//			double wa = weights.get(a);
			int ia = e.getAgentIdx(a);
			Set<Agent> c = new HashSet<>();
			
			c.add(a);
			
			e.relations().filter(r -> r.includes(ia)).forEach(r -> {
				Agent b = e.getAgent(r.other(ia));
				
				if(open.contains(b))
				{
					double thresh = 0;//.5 / Math.sqrt(wa + weights.get(b));
					
					if(e.orientation(r) >= thresh)
					{
						c.add(b);
						open.remove(b);
					}
				}
			});
			
			if(c.size() > 1)
			{
				cluster.add(c);
			}
		}
		
		return cluster;
	}
}
