package cas.work;

import dave.json.JsonConstant;
import dave.json.JsonValue;
import dave.json.Loadable;
import dave.json.Saveable;

public class RivalsOWWatcher implements Watcher, Saveable, Loadable
{
	@Override
	public boolean evaluate(Framework f)
	{
		Double ow = f.get("rivals-ow");
		
		return ow < 0;
	}

	@Override
	public void load(JsonValue json)
	{
	}

	@Override
	public JsonValue save()
	{
		return JsonConstant.NULL;
	}
}
