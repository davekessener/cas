package cas.work;

import dave.json.Container;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;

@Container
public class DeviationWatcher implements Watcher, Saveable
{
	private final double mThresh;
	
	public DeviationWatcher(double t)
	{
		mThresh = t;
	}
	
	@Override
	public boolean evaluate(Framework f)
	{
		ValueAverageParam.Result r = f.get("val-avg");
		
		return Math.abs(r.mean_deviation) >= mThresh;
	}
	
	@Override
	@Saver
	public JsonValue save( )
	{
		JsonObject json = new JsonObject();
		
		json.putNumber("threshold", mThresh);
		
		return json;
	}
	
	@Loader
	public static DeviationWatcher load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		double threshold = o.getDouble("threshold");
		
		return new DeviationWatcher(threshold);
	}
}
