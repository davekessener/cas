package cas.work;

import dave.util.Identifiable;

public interface Keeper extends Identifiable
{
	public abstract String process(Framework f);
}
