package cas.work;

public class StepParameter extends BaseParameter
{
	private final int mStep;
	
	public StepParameter(int s)
	{
		super("step");
		
		mStep = s;
	}

	@Override
	public Object calculate(Framework fw)
	{
		return mStep;
	}
}
