package cas.work;

public abstract class BaseParameter implements Parameter
{
	private final String mID;
	
	protected BaseParameter(String id)
	{
		mID = id;
	}
	
	@Override
	public String getID( )
	{
		return mID;
	}
}
