package cas.work;

public interface Watcher
{
	public abstract boolean evaluate(Framework f);
}
