package cas.work;

import java.util.List;

import cas.detail.Agent;
import cas.detail.Environment;
import cas.detail.Relation;

public class RivalsOWParam extends BaseParameter
{
	public RivalsOWParam( )
	{
		super("rivals-ow");
	}

	@Override
	public Object calculate(Framework fw)
	{
		Environment e = fw.get("environment");
		List<Agent> cores = fw.get("cores");
		
		if(cores.size() != 2)
			return Double.NEGATIVE_INFINITY;
		
		int ai = e.getAgentIdx(cores.get(0));
		int bi = e.getAgentIdx(cores.get(1));
		
		Relation r = e.relations().filter(rr -> rr.includes(ai) && rr.includes(bi)).findAny().orElse(null);
		
		if(r == null)
			return Double.NEGATIVE_INFINITY;
		
		return e.orientation(r);
	}
}
