package cas.work;

public abstract class BaseKeeper implements Keeper
{
	private final String mID;
	
	protected BaseKeeper(String id)
	{
		mID = id;
	}
	
	@Override
	public String getID( )
	{
		return mID;
	}
}
