package cas.work;

public class DurationParam extends BaseParameter
{
	private final long mStart;
	
	public DurationParam( )
	{
		super("duration");
		
		mStart = System.currentTimeMillis();
	}

	@Override
	public Object calculate(Framework fw)
	{
		return System.currentTimeMillis() - mStart;
	}
}
