package cas.work;

import java.util.function.Function;

public class SimpleKeeper<T> extends BaseKeeper
{
	private final String mID;
	private final Function<T, String> mCallback;
	
	public SimpleKeeper(String name, String id, Function<T, String> f)
	{
		super(name);
		mID = id;
		mCallback = f;
	}
	
	@Override
	public String process(Framework f)
	{
		return mCallback.apply(f.get(mID));
	}
}
