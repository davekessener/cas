package cas.work;

import dave.json.JsonConstant;
import dave.json.JsonValue;
import dave.json.Loadable;
import dave.json.Saveable;

public class EmptyWatcher implements Watcher, Saveable, Loadable
{
	@Override
	public boolean evaluate(Framework f)
	{
		return true;
	}

	@Override
	public void load(JsonValue json)
	{
	}

	@Override
	public JsonValue save()
	{
		return JsonConstant.NULL;
	}
}
