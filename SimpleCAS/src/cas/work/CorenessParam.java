package cas.work;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import cas.detail.Environment;

public class CorenessParam extends BaseParameter
{
	public CorenessParam( )
	{
		super("coreness");
	}

	@Override
	public Object calculate(Framework fw)
	{
		Environment e = fw.get("environment");
		List<Set<Integer>> cores = new ArrayList<>();
		List<Integer> current = IntStream.range(0, (int) e.agents().count()).mapToObj(Integer::valueOf).collect(Collectors.toList());
		int i = 0;
		
		while(!current.isEmpty())
		{
			++i;
			
			Set<Integer> found = new HashSet<>();
			List<Integer> next = new ArrayList<>();
			for(Integer a : current)
			{
				int c = (int) e.relations().filter(r -> r.includes(a)).count();
				
				(c <= i ? found : next).add(a);
			}
			
			cores.add(found);
			current = next;
		}
		
		return cores;
	}
}
