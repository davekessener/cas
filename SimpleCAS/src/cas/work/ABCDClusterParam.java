package cas.work;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.ToDoubleFunction;
import java.util.stream.Collectors;

import cas.detail.Agent;
import cas.detail.Environment;
import cas.detail.Relation;

public class ABCDClusterParam extends BaseParameter
{
	public ABCDClusterParam( )
	{
		super("abcd-cluster");
	}

	@Override
	public Object calculate(Framework fw)
	{
		Environment e = fw.get("environment");
		List<Set<Agent>> cluster = new ArrayList<>();
		Map<Agent, Double> node_weights = new HashMap<>();
		Map<Relation, Double> edge_weights = new HashMap<>();
		
		e.agents().forEach(a -> {
			Set<Agent> c = new HashSet<>();
			
			c.add(a);
			cluster.add(c);
			node_weights.put(a, e.value(a));
		});
		e.relations().forEach(r -> edge_weights.put(r, e.orientation(r)));
		
		double max = Math.max(1, node_weights.entrySet().stream().mapToDouble(et -> et.getValue()).max().getAsDouble());
		ToDoubleFunction<Set<Agent>> clusterWeight = c -> c.stream().mapToDouble(node_weights::get).sum() / (max * c.size());
		
		while(true)
		{
			int merge_a = -1, merge_b = -1;
			double merge_attract = 0;
			
			for(int i = 0 ; i < cluster.size() ; ++i)
			{
				Set<Agent> a = cluster.get(i);
				double a_weight = clusterWeight.applyAsDouble(a);
				
				for(int j = i + 1 ; j < cluster.size() ; ++j)
				{
					Set<Agent> b = cluster.get(j);
					Set<Relation> edges = e.relations().filter(r -> {
						Agent left = e.getAgent(r.left);
						Agent right = e.getAgent(r.right);
						
						return (a.contains(left) && b.contains(right))
							|| (a.contains(right) && b.contains(left));
					}).collect(Collectors.toSet());
					
					if(edges.size() < a.size() || edges.size() < b.size()) // not inter-interested
						continue;
					
					double b_weight = clusterWeight.applyAsDouble(b);
					double attract = edges.stream().mapToDouble(edge_weights::get).sum() / (a.size() * b.size());
					
					if(attract >= (a_weight + b_weight) / 2)
					{
						if(merge_a == -1 || attract > merge_attract)
						{
							merge_a = i;
							merge_b = j;
							merge_attract = attract;
						}
					}
				}
			}
			
			if(merge_a != -1)
			{
				Set<Agent> a = cluster.get(merge_a);
				Set<Agent> b = cluster.get(merge_b);
				
				b.forEach(a::add);
				cluster.remove(b);
				
				continue;
			}
			
			break;
		}
		
		return cluster;
	}
}
