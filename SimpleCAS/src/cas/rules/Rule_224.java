package cas.rules;

import java.util.stream.IntStream;

import cas.common.EdgeRule;
import cas.detail.Environment;
import cas.detail.OrderedEdge;
import cas.detail.Relation;

public class Rule_224 extends BaseRule implements EdgeRule
{
	public Rule_224( )
	{
		super(Rules.ID.RULE_224.toString(), e -> true);
	}

	@Override
	public void apply(Environment e, Relation r)
	{
		OrderedEdge o = new OrderedEdge(e, r);
		double vo = o.bigger.value;
		double chance = vo / (vo + o.smaller.value);
		
		if(e.random().nextDouble() < chance)
		{
			IntStream.range(0, (int) e.agents().count())
				.filter(i -> (i != r.left && i != r.right
					&& e.relations().anyMatch(rr -> rr.equals(new Relation(i, r.left)))
					&& e.relations().anyMatch(rr -> rr.equals(new Relation(i, r.right)))))
				.mapToObj(i -> new Relation(i, o.bigger.index))
				.forEach(rr -> {
					int oo = rr.other(o.bigger.index);
					double vn = e.value(e.getAgent(oo));
					
					if(e.random().nextDouble() < vo / (vo + vn))
					{
						e.disconnect(o.smaller.index, oo);
					}
				});
		}
	}
}
