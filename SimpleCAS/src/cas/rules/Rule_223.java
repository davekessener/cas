package cas.rules;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import cas.common.EdgeRule;
import cas.detail.Configuration;
import cas.detail.Environment;
import cas.detail.Relation;

public class Rule_223 extends BaseRule implements EdgeRule
{
	private final boolean mCanSplit;
	
	public Rule_223( )
	{
		super(Rules.ID.RULE_223.toString(), e -> true);
		
		mCanSplit = Configuration.DISCONNECTABLE.getValue();
	}

	@Override
	public void apply(Environment e, Relation r)
	{
		double chance = 1.0 - Math.abs(e.orientation(r));
		
		chance = chance * chance - 0.1;
		
		if(e.random().nextDouble() < chance && (mCanSplit || reachable(e, r)))
		{
			e.disconnect(r.left, r.right);
		}
	}
	
	private boolean reachable(Environment e, Relation r)
	{
		Set<Integer> reached = new HashSet<>();
		Set<Integer> current = new HashSet<>();
		
		reached.add(r.left);
		current.add(r.left);
		
		while(!current.isEmpty())
		{
			Iterator<Integer> i = current.iterator();
			int ci = i.next();
			
			i.remove();
			
			boolean found = e.relations().filter(rr -> r != rr && rr.includes(ci)).filter(rr -> {
				int j = rr.other(ci);
				
				if(j == r.right)
					return true;
				
				if(!reached.contains(j))
				{
					reached.add(j);
					current.add(j);
				}
				
				return false;
			}).findAny().isPresent();
			
			if(found) return true;
		}
		
		return false;
	}
}
