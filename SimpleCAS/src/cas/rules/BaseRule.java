package cas.rules;

import cas.common.Filter;
import cas.common.Rule;
import cas.detail.Environment;

public class BaseRule implements Rule
{
	private final String mID;
	private final Filter mFilter;
	
	protected BaseRule(String id, Filter f)
	{
		mID = id;
		mFilter = f;
	}
	
	@Override
	public boolean applicable(Environment e)
	{
		return mFilter.test(e);
	}

	@Override
	public String getID()
	{
		return mID;
	}
}
