package cas.rules;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Queue;
import java.util.Set;

import cas.common.EdgeRule;
import cas.detail.Agent;
import cas.detail.Environment;
import cas.detail.OrderedEdge;
import cas.detail.Relation;

public class Rule_222 extends BaseRule implements EdgeRule
{
	public Rule_222( )
	{
		super(Rules.ID.RULE_222.toString(), e -> true);
	}

	@Override
	public void apply(Environment e, Relation r)
	{
		OrderedEdge o = new OrderedEdge(e, r);
		
		if(o.orientation > 0 && e.random().nextDouble() < o.orientation)
		{
			Queue<Integer> open = new ArrayDeque<>();
			Set<Integer> done = new HashSet<>();
			
			open.add(o.smaller.index);
			done.add(o.bigger.index);
			
			while(!open.isEmpty())
			{
				int i = open.poll();
				
				done.add(i);
				
				List<Integer> c = candidates(e, o.bigger.index, o.bigger.value, i);
				
				if(c.isEmpty())
				{
					e.relations()
						.filter(rr -> rr.includes(i))
						.filter(rr -> !done.contains(rr.other(i)))
						.forEach(rr -> open.add(rr.other(i)));
				}
				else
				{
					c.forEach(j -> e.connect(o.bigger.index, j));
					
					break;
				}
			}
		}
	}
	
	private static List<Integer> candidates(Environment e, int bidx, double bval, int sidx)
	{
		List<Integer> to_be_added = new ArrayList<>();
		
		e.relations().filter(rr -> (rr.includes(sidx) && !rr.includes(bidx))).forEach(rr -> {
			int i_o = rr.other(sidx);
			Agent a_o = e.getAgent(i_o);
			double v_o = e.value(a_o);
			
			double chance = bval / v_o;
			
			if(chance >= 1.0 || e.random().nextDouble() < chance)
			{
				to_be_added.add(i_o);
			}
		});
		
		return to_be_added;
	}
}
