package cas.rules;

import java.util.Map;

import cas.common.Rule;
import dave.util.ImmutableMapBuilder;
import dave.util.Producer;

public final class Rules
{
	public static enum ID
	{
		RULE_211,
		RULE_212,
		RULE_213,
		RULE_222,
		RULE_223,
		RULE_224
	}
	
	private static final Map<ID, Producer<Rule>> sRules = (new ImmutableMapBuilder<ID, Producer<Rule>>())
		.put(ID.RULE_211, () -> new Rule_211())
		.put(ID.RULE_212, () -> new Rule_212())
		.put(ID.RULE_213, () -> new Rule_213())
		.put(ID.RULE_222, () -> new Rule_222())
		.put(ID.RULE_223, () -> new Rule_223())
		.put(ID.RULE_224, () -> new Rule_224())
		.build();

	public static Rule get(String id) { return sRules.get(ID.valueOf(id)).produce(); }
	
	private Rules( ) { }
}
