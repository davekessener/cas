package cas.rules;

import java.util.stream.IntStream;

import cas.common.EdgeRule;
import cas.common.Filter;
import cas.detail.Agent;
import cas.detail.Configuration;
import cas.detail.Environment;
import cas.detail.Relation;

public class Rule_211 extends BaseRule implements EdgeRule
{
	public Rule_211( )
	{
		super(Rules.ID.RULE_211.toString(), new Filter.Constant(Configuration.FACTOR_211.getValue()));
	}

	@Override
	public void apply(Environment e, Relation r)
	{
		Agent a = e.getAgent(r.left);
		Agent b = e.getAgent(r.right);
		boolean larger = e.value(a) > e.value(b);
		
		IntStream.range(0, e.size()).forEach(i -> {
			double f = 1.0;
			double d = Configuration.R211_DIFF.getValue();
			double ai = a.get(i), bi = b.get(i);
			
			if(ai * bi < 0)
			{
				f -= d;
			}
			else
			{
				f += d;
			}
			
			if(larger) bi = lerp(bi, ai, d);
			else ai = lerp(ai, bi, d);
			
			a.set(i, ai * f);
			b.set(i, bi * f);
		});
	}
	
	private static double lerp(double a, double b, double p)
	{
		return a + (b - a) * p;
	}
}
