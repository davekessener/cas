package cas.rules;

import java.util.stream.IntStream;

import cas.common.NodeRule;
import cas.detail.Agent;
import cas.detail.Configuration;
import cas.detail.Environment;

public class Rule_213 extends BaseRule implements NodeRule
{
	public Rule_213( )
	{
		super(Rules.ID.RULE_213.toString(), f -> true);
	}

	@Override
	public void apply(Environment e, Agent a)
	{
		double s = e.spectrum(a);
		
		if(s > 0)
		{
			double f = Configuration.FACTOR_213.getValue();
			
			if(e.random().nextDouble() < e.value(a) / (s * f))
			{
				addNewInterest(e, a);
			}
		}
	}
	
	private static void addNewInterest(Environment e, Agent a)
	{
		int aidx = e.getAgentIdx(a);
		int max[] = { -1, -1 };

		IntStream.range(0, e.size()).forEach(i -> {
			if(a.get(i) == 0)
			{
				int c = (int) e.relations()
					.filter(r -> r.includes(aidx))
					.filter(r -> (e.orientation(r) > 0))
					.filter(r -> (e.getAgent(r.other(aidx)).get(i) == 0))
					.count();
				
				if(c > max[1])
				{
					max[0] = i;
					max[1] = c;
				}
			}
		});
		
		if(max[0] >= 0)
		{
			a.set(max[0], (2 * e.random().nextDouble() - 1) * 10);
		}
	}
}
