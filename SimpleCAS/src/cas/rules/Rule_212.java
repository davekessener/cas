package cas.rules;

import cas.common.EdgeRule;
import cas.common.Filter;
import cas.detail.Configuration;
import cas.detail.Environment;
import cas.detail.OrderedEdge;
import cas.detail.Relation;

public class Rule_212 extends BaseRule implements EdgeRule
{
	public Rule_212( )
	{
		super(Rules.ID.RULE_212.toString(), new Filter.Constant(Configuration.FACTOR_212.getValue()));
	}

	@Override
	public void apply(Environment e, Relation r)
	{
		double chance = -e.orientation(r);
		
		if(chance >= 1.0 || (chance > 0.0 && e.random().nextDouble() < chance))
		{
			OrderedEdge o = new OrderedEdge(e, r);
			int idx = -1;
			double diff = 0;
			
			for(int i = 0 ; i < e.size() ; ++i)
			{
				double d = o.bigger.agent.get(i) - o.smaller.agent.get(i);
				
				if(idx < 0 || Math.abs(d) > Math.abs(diff))
				{
					idx = i;
					diff = d;
				}
			}
			
			if(e.random().nextDouble() < o.bigger.value / (o.bigger.value + o.smaller.value))
			{
				o.smaller.agent.set(idx, o.smaller.agent.get(idx) + Configuration.R212_DIST.getValue() * diff);
			}
		}
	}
}
