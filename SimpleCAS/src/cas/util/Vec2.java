package cas.util;

import dave.json.Container;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;

@Container
public class Vec2 implements Saveable
{
	public final int x, y;
	
	public Vec2(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
	
	@Override
	public int hashCode( )
	{
		return Integer.hashCode(x) ^ (Integer.hashCode(y) * 13);
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof Vec2)
		{
			Vec2 v = (Vec2) o;
			
			return x == v.x && y == v.y;
		}
		
		return false;
	}
	
	@Override
	@Saver
	public JsonValue save( )
	{
		JsonObject json = new JsonObject();

		json.putInt("x", x);
		json.putInt("y", y);
		
		return json;
	}
	
	@Loader
	public static Vec2 load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		int x = o.getInt("x");
		int y = o.getInt("y");
		
		return new Vec2(x, y);
	}
}
