package cas.util;

public interface Observable
{
	public void addListener(Observer o);
}
