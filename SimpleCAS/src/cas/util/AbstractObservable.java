package cas.util;

import java.util.ArrayList;
import java.util.List;

public class AbstractObservable implements Observable
{
	private final List<Observer> mListeners = new ArrayList<>();

	@Override
	public void addListener(Observer o)
	{
		mListeners.add(o);
	}

	protected void change( )
	{
		for(Observer o : mListeners)
		{
			o.onChange(this);
		}
	}
}
