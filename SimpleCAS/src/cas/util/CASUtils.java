package cas.util;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.function.Consumer;

import cas.detail.Configuration;
import cas.work.KeeperManager;
import cas.work.Supervisor;
import cas.work.Watcher;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.util.SevereException;

public final class CASUtils
{
	public static JsonValue merge(JsonValue b, JsonValue c)
	{
		if((c instanceof JsonObject) && (b instanceof JsonObject))
		{
			JsonObject r = new JsonObject();
			JsonObject base = (JsonObject) b;
			JsonObject change = (JsonObject) c;
			
			base.keySet().forEach(id -> r.put(id, base.get(id)));
			
			change.keySet().forEach(key -> {
				JsonValue o = change.get(key);
				
				if(r.contains(key))
				{
					o = merge(r.get(key), o);
				}
				
				r.put(key, o);
			});
			
			return r;
		}
		else
		{
			return c;
		}
	}
	
	public static Watcher generateWatcher(OutputStream log)
	{
		final Consumer<String> proxy = s -> {
			try
			{
				log.write(s.getBytes());
				log.write("\n".getBytes());
				log.flush();
			}
			catch(IOException e)
			{
				throw new SevereException(e);
			}
		};
		
		final KeeperManager keepers = new KeeperManager(proxy);
		final Watcher watcher = Configuration.get(Configuration.WATCHER.getValue());
		final Watcher supervisor = new Supervisor(Configuration.MIN_CYCLES.getValue(), Configuration.MAX_CYCLES.getValue(), watcher);
		
		Arrays.stream(Configuration.LOGGERS.getValue()).forEach(keepers::add);
		
		return fw -> {
			keepers.accept(fw);
			
			return supervisor.evaluate(fw);
		};
	}
	
	private CASUtils( ) { }
}
