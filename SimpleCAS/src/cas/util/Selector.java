package cas.util;

public class Selector<T>
{
	public static interface Comparer<T> { boolean compare(T o, T n); }
	
	private final Comparer<T> mComp;
	private T mObj;
	
	public Selector(T o, Comparer<T> c)
	{
		mComp = c;
		mObj = o;
	}
	
	public T get( ) { return mObj; }
	public void set(T o) { mObj = o; }
	
	public Selector<T> add(T v)
	{
		if(mComp.compare(mObj, v))
		{
			mObj = v;
		}
		
		return this;
	}
}
