package cas.util;

public class SimpleProperty<T> extends AbstractObservable implements ObservableProperty<T>
{
	private T mObject;
	
	public SimpleProperty( ) { this(null); }
	public SimpleProperty(T o)
	{
		mObject = o;
	}
	
	@Override
	public void setValue(T v)
	{
		mObject = v;
		
		change();
	}
	
	@Override
	public T getValue()
	{
		return mObject;
	}
}
