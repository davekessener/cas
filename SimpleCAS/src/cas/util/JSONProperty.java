package cas.util;

import dave.json.JSON;
import dave.json.JsonValue;
import dave.json.Loadable;
import dave.json.Saveable;

public class JSONProperty<T> extends SimpleProperty<T> implements Saveable, Loadable
{
	public JSONProperty( ) { }
	public JSONProperty(T v) { super(v); }
	
	@SuppressWarnings("unchecked")
	@Override
	public void load(JsonValue json)
	{
		setValue((T) JSON.deserialize(json));
	}

	@Override
	public JsonValue save()
	{
		return JSON.serialize(getValue());
	}
}
