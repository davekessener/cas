package cas.util;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class LinReg
{
	private final int mSamples;
	private final float mDeviation;
	private final List<Entry> mGraph;
	private final Cache mCache;
	
	public LinReg(int n, float d)
	{
		mSamples = n;
		mDeviation = d;
		mGraph = new ArrayList<>();
		mCache = new Cache();
	}
	
	public void add(int x, int y)
	{
		if(mGraph.isEmpty() || mCache.last_y != y)
		{
			mGraph.add(new Entry(x, mCache.last_y = y));
			mCache.dirty = true;
		}
	}
	
	public boolean done( )
	{
		boolean r = false;
		
		if(!mCache.dirty)
		{
			r = mCache.value;
		}
		else if(mGraph.size() >= mSamples)
		{
			r = (compute() < mDeviation);
		}
		
		mCache.value = r;
		mCache.dirty = false;
		
		return r;
	}
	
	private float compute( )
	{
		Entry[] t = new Entry[mSamples];
		int a = 0, b = mGraph.size() - 1;
		
		t[mSamples - 1] = mGraph.get(b);
		
		for(int i = 0 ; i < t.length - 1 ; ++i)
		{
			t[i] = mGraph.get(a);
			a = (a + b) / 2;
		}
		
		float[] m = { 0.0f, 0.0f };
		float[] ss = { 0.0f, 0.0f };
		float n = mSamples;
		
		Stream.of(t).forEach(e -> {
			m[0] += e.x;
			m[1] += e.y;
			
			ss[0] += e.x * e.y;
			ss[1] += e.x * e.x;
		});
		
		m[0] /= n;
		m[1] /= n;
		ss[0] -= n * m[0] * m[1];
		ss[1] -= n * m[0] * m[0];
		
		System.out.println("Slope is " + (ss[0] / ss[1]));
		
		return ss[0] / ss[1];
	}
	
	private static class Cache
	{
		private int last_y;
		private boolean dirty = true;
		private boolean value;
	}
	
	private static class Entry
	{
		public final int x, y;
		
		public Entry(int x, int y)
		{
			this.x = x;
			this.y = y;
		}
	}
}
