package cas.util;

public interface Observer
{
	public void onChange(Observable o);
}
