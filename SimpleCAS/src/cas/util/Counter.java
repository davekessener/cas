package cas.util;

public interface Counter
{
	public abstract int next( );
	
	public static class FibCounter implements Counter
	{
		int mLast = 0, mNext = 1;
		
		@Override
		public int next( )
		{
			mLast = mNext - mLast;
			mNext += mLast;
			
			return mLast;
		}
	}
}
