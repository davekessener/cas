package cas.util;

import dave.util.RNG;
import dave.util.XoRoRNG;

public class UniformRange
{
	private final RNG mRand;
	private final long mOffset, mRange, mMax;
	
	public UniformRange(long min, long max) { this(min, max, new XoRoRNG()); }
	public UniformRange(long min, long max, RNG rand)
	{
		if(min >= max)
			throw new IllegalArgumentException(String.format("UniformRange [%d, %d)", min, max));
		
		mRand = rand;
		mOffset = min;
		mRange = max - min;
		mMax = Long.MAX_VALUE - (Long.MAX_VALUE % mRange);
	}
	
	public long next( )
	{
		long v = 0;
		
		do
		{
			v = mRand.nextLong();
		}
		while(v >= mMax);
		
		return mOffset + ((v % mRange) + mRange) % mRange;
	}
}
