package cas.util;

public interface Property<T>
{
	public abstract void setValue(T v);
	public abstract T getValue( );
}
