package cas.util;

public interface ObservableProperty<T> extends Property<T>, Observable
{
}
