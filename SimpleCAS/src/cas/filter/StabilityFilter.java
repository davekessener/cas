package cas.filter;

public class StabilityFilter extends ChanceFilter
{
	public StabilityFilter(double m, double a)
	{
		super(e -> {
			double x = (e.agents().count() / (double) e.relations().count());
			
			return 0.5 - m * (x - a);
		});
	}
}
