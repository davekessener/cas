package cas.filter;

import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;

import cas.common.Filter;
import cas.detail.Environment;

public class ChanceFilter implements Filter
{
	private final ToDoubleFunction<Environment> mF;
	
	public ChanceFilter(int min, int max, ToIntFunction<Environment> f)
	{
		this(e -> Math.max(0, Math.min((f.applyAsInt(e) - min) / (double) (max - min), 1)));
	}
	
	public ChanceFilter(double f) { this(e -> f); }
	public ChanceFilter(ToDoubleFunction<Environment> f)
	{
		mF = f;
	}

	@Override
	public boolean test(Environment e)
	{
		double chance = mF.applyAsDouble(e);
		
		return (chance > 0 && (chance >= 1.0 || e.random().nextDouble() < chance));
	}
}
