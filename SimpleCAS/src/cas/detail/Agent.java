package cas.detail;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

import dave.json.Container;
import dave.json.JsonArray;
import dave.json.JsonCollectors;
import dave.json.JsonObject;
import dave.json.JsonString;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;

@Container
public class Agent implements Saveable
{
	private final double[] mInterests;
	private final List<String> mTags;
	
	public Agent(int n)
	{
		mInterests = new double[n];
		mTags = new ArrayList<>();
	}
	
	public Agent(double[] v)
	{
		this(v.length);
		
		for(int i = 0 ; i < v.length ; ++i)
		{
			set(i, v[i]);
		}
	}
	
	public Agent tag(String tag) { mTags.add(tag); return this; }
	public boolean hasTag(String tag) { return mTags.stream().anyMatch(tag::equalsIgnoreCase); }
	public String[] tags( ) { return mTags.toArray(new String[mTags.size()]); }
	public DoubleStream interests( ) { return DoubleStream.of(mInterests); }
	
	public int size( ) { return mInterests.length; }
	public double get(int i) { return mInterests[i]; }
	public void set(int i, double v) { mInterests[i] = Math.max(-1, Math.min(v, 1)); }
	
	@Override
	public String toString( )
	{
		return "Agent(" + mTags.stream().collect(Collectors.joining(", ")) + ")";
	}
	
	@Override
	@Saver
	public JsonValue save( )
	{
		JsonObject json = new JsonObject();
		
		json.put("interests", new JsonArray(mInterests));
		json.put("tags", mTags.stream().map(JsonString::new).collect(JsonCollectors.ofArray()));
		
		return json;
	}
	
	@Loader
	public static Agent load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		Agent a = new Agent(o.getArray("interests").asDoubles());
		
		o.getArray("tags").stream()
			.map(v -> ((JsonString) v).get())
			.forEach(a::tag);
		
		return a;
	}
}
