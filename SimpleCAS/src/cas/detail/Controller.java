package cas.detail;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import cas.common.EdgeRule;
import cas.common.Generator;
import cas.common.NodeRule;
import cas.common.Rule;
import cas.rules.Rules;
import cas.util.Counter;
import cas.util.Counter.FibCounter;
import cas.util.UniformRange;
import cas.work.DurationParam;
import cas.work.Framework;
import cas.work.Parameter;
import cas.work.SimpleFramework;
import cas.work.StepParameter;
import cas.work.Watcher;
import dave.json.Container;
import dave.json.JsonArray;
import dave.json.JsonCollectors;
import dave.json.JsonObject;
import dave.json.JsonString;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;
import dave.util.SevereException;
import dave.util.log.Logger;
import dave.util.log.Severity;

@Container
public class Controller implements Saveable
{
	private final Environment mEnv;
	private final List<Rule> mRules;
	private final List<Parameter> mParams;
	
	public Controller(int n)
	{
		mEnv = new Environment(n);
		mRules = new ArrayList<>();
		mParams = new ArrayList<>();
	}
	
	public Environment environment( ) { return mEnv; }
	
	public void generate(Generator g)
	{
		mEnv.reset();
		g.accept(mEnv);
	}
	
	public Controller add(Rule r)
	{
		mRules.add(r);
		
		return this;
	}
	
	public Controller add(Parameter p)
	{
		mParams.add(p);
		
		return this;
	}
	
	public void run(Watcher w)
	{
		if(Configuration.STRICT.getValue())
		{
			runSync(w);
		}
		else
		{
			runAsync(w);
		}
	}
	
	private void runSync(Watcher w)
	{
		Parameter time = new DurationParam();
		int step = 0;
		
		while(true)
		{
			Framework fw = SimpleFramework.construct("environment", mEnv, mParams, time, new StepParameter(step));
			
			if(!w.evaluate(fw)) break;
			
			step();
			
			++step;
		}

		LOG.log("Terminated after %d steps.", step);
	}
	
	private void runAsync(Watcher w)
	{
		ExecutorService async = Executors.newSingleThreadExecutor();
		Parameter time = new DurationParam();
		Environment last = null;
		
		class Evaluator implements Callable<Boolean>
		{
			private final Framework mFramework;
			
			public Evaluator(int s, Environment e)
			{
				mFramework = SimpleFramework.construct("environment", e, mParams, new StepParameter(s), time);
			}
			
			@Override
			public Boolean call()
			{
				return w.evaluate(mFramework);
			}
		}
		
		int step = 0;
		int check = 1;
		Counter c = new FibCounter();
		
		try
		{
			boolean running = (new Evaluator(step, mEnv)).call();
			Future<Boolean> f = null;
//			long t = System.currentTimeMillis();
			
			try
			{
				while(running)
				{
					boolean needs_check = false;
					
					step();
					++step;
					
					if(f == null)
					{
//						if(System.currentTimeMillis() - t > 10)
						{
							f = async.submit(new Evaluator(step, last = mEnv.clone()));
						}
					}
					else
					{
						needs_check = (needs_check || f.isDone());
					}
					
					needs_check = (needs_check || (step >= check));
					
					if(f != null && needs_check)
					{
						check = step + c.next();
						running = f.get();
						f = null;
//						t = System.currentTimeMillis();
					}
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
				throw new SevereException(e);
			}
			
			if(last != null)
			{
				mEnv.load(last.save());
			}
		}
		finally
		{
			async.shutdownNow();
		}
		
		LOG.log("Terminated after %d steps.", step);
	}
	
	public void step( )
	{
		List<Integer> idx = IntStream.range(0, mRules.size()).mapToObj(Integer::valueOf).collect(Collectors.toList());
		
		Collections.shuffle(idx, new Random(mEnv.random().nextLong()));
		
		idx.stream().map(mRules::get).forEach(r -> {
			if(mEnv.agents().count() == 0 || mEnv.relations().count() == 0)
				throw new IllegalStateException();
			
			UniformRange a = new UniformRange(0, mEnv.agents().count(), mEnv.random());
			UniformRange e = new UniformRange(0, mEnv.relations().count(), mEnv.random());
			
			if(r.applicable(mEnv))
			{
				if(r instanceof NodeRule)
				{
					((NodeRule) r).apply(mEnv, mEnv.getAgent((int) a.next()));
				}
				else if(r instanceof EdgeRule)
				{
					((EdgeRule) r).apply(mEnv, mEnv.getRelation((int) e.next()));
				}
				else
				{
					LOG.log(Severity.FATAL, "Unknown rule: %s", r.getClass().getName());
				}
			}
		});
	}
	
	public JsonValue graph( )
	{
		JsonObject json = new JsonObject();
		
		json.put("nodes", mEnv.agents().map(a -> {
			JsonObject o = new JsonObject();
			String[] tags = a.tags();
			
			o.putNumber("value", mEnv.value(a));
			o.put("tags", new JsonArray(tags));
			
			return o;
		}).collect(JsonCollectors.ofArray()));
		
		json.put("edges", mEnv.relations().map(r -> {
			JsonObject o = new JsonObject();
			
			o.putInt("left", r.left);
			o.putInt("right", r.right);
			o.putNumber("weight", mEnv.orientation(r));
			
			return o;
		}).collect(JsonCollectors.ofArray()));
		
		return json;
	}

	@Override
	@Saver
	public JsonValue save()
	{
		JsonObject json = new JsonObject();
		
		json.putInt("size", mEnv.size());
		json.put("environment", mEnv.save());
		json.put("rules", mRules.stream().map(r -> new JsonString(r.getID())).collect(JsonCollectors.ofArray()));

		return json;
	}

	@Loader
	public static Controller load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		Controller ctrl = new Controller(o.getInt("size"));
		
		ctrl.mEnv.load(o.get("environment"));
		o.getArray("rules").forEach(v -> ctrl.mRules.add(Rules.get(((JsonString) v).get())));
		
		return ctrl;
	}
	
	private static final Logger LOG = Logger.get("ctrl");
}
