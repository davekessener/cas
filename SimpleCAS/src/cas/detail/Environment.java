package cas.detail;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import dave.json.Container;
import dave.json.JsonCollectors;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loadable;
import dave.json.Saveable;
import dave.json.Saver;
import dave.util.RNG;
import dave.util.Seed;
import dave.util.XoRoRNG;

@Container
public class Environment implements Saveable, Loadable, Cloneable
{
	private final int mSize;
	private final List<Agent> mAgents;
	private final List<Relation> mRelations;
	private final RNG mRandom;
	
	public Environment(int n)
	{
		mSize = n;
		mAgents = new ArrayList<>();
		mRelations = new ArrayList<>();
		mRandom = new XoRoRNG();
	}
	
	public Stream<Agent> agents( ) { return mAgents.stream(); }
	public Stream<Relation> relations( ) { return mRelations.stream(); }
	public Agent getAgent(int i) { return mAgents.get(i); }
	public Relation getRelation(int i) { return mRelations.get(i); }
	public RNG random( ) { return mRandom; }
	public int size( ) { return mSize; }
	public int getAgentIdx(Agent a) { return mAgents.indexOf(a); }
	
	public void reset( )
	{
		mAgents.clear();
		mRelations.clear();
	}
	
	public Agent create(Agent a, double c)
	{
		Agent b = create();
		
		for(int i = 0 ; i < size() ; ++i)
		{
			if(random().nextDouble() < c)
			{
				b.set(i, a.get(i));
			}
		}
		
		return b;
	}
	
	public void connect(int l, int r)
	{
		Relation rr = new Relation(l, r);
		
		if(!mRelations.contains(rr))
		{
			mRelations.add(rr);
		}
	}
	
	public void disconnect(int l, int r)
	{
		Relation rr = new Relation(l, r);
		
		mRelations.remove(rr);
	}
	
	public double spectrum(Agent a)
	{
		return a.interests().map(v -> v * v).sum();
	}
	
	public double orientation(Relation r)
	{
		Agent a = mAgents.get(r.left);
		Agent b = mAgents.get(r.right);
		
		return orientation(a, b);
	}
	
	public double orientation(Agent a, Agent b)
	{
		return IntStream.range(0, mSize).mapToDouble(i -> a.get(i) * b.get(i)).sum() / mSize;
	}
	
	public double value(Agent a)
	{
		int i = mAgents.indexOf(a);
		
		return mRelations.stream().filter(r -> r.includes(i)).mapToDouble(this::orientation).map(v -> v * v).sum();
	}
	
	@Override
	public Environment clone( )
	{
		Environment e = new Environment(mSize);
		
		mAgents.forEach(e.mAgents::add);
		mRelations.forEach(e.mRelations::add);
		e.mRandom.setSeed(mRandom.getSeed());
		
		return e;
	}
	
	@Override
	@Saver
	public JsonValue save( )
	{
		JsonObject json = new JsonObject();
		
		json.putInt("size", mSize);
		json.put("agents", mAgents.stream().map(Agent::save).collect(JsonCollectors.ofArray()));
		json.put("relations", mRelations.stream().map(Relation::save).collect(JsonCollectors.ofArray()));
		json.putString("seed", mRandom.getSeed().toString());
		
		return json;
	}
	
	@Override
	public void load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		if(o.getInt("size") != mSize)
			throw new IllegalArgumentException("Incompatible sizes: " + mSize);
		
		mAgents.clear();
		mRelations.clear();
		
		o.getArray("agents").stream().map(Agent::load).forEach(a -> mAgents.add(a));
		o.getArray("relations").stream().map(Relation::load).forEach(r -> mRelations.add(r));
		
		mRandom.setSeed(new Seed(o.getString("seed")));
	}
	
	public Agent create( )
	{
		Agent a = new Agent(mSize);
		
		mAgents.add(a);
		
		return a;
	}
	
	public Relation connect(Agent a, Agent b)
	{
		Relation r = new Relation(mAgents.indexOf(a), mAgents.indexOf(b));
		
		if(!mRelations.stream().filter(r::equals).findAny().isPresent())
		{
			mRelations.add(r);
		}
		
		return r;
	}
}
