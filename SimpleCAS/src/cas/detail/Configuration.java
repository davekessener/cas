package cas.detail;

import java.util.HashMap;
import java.util.Map;

import cas.gen.AsymmetryGenerator;
import cas.gen.RandomGenerator;
import cas.gen.StarRivalsGenerator;
import cas.gen.TriangleGenerator;
import cas.gen.UniformGenerator;
import cas.util.CASUtils;
import cas.util.JSONProperty;
import cas.util.Property;
import cas.work.ChangeWatcher;
import cas.work.DeviationWatcher;
import cas.work.EmptyWatcher;
import cas.work.RivalsOWWatcher;
import cas.work.SingularityWatcher;
import dave.json.JsonCollectors;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.util.log.Logger;
import dave.util.log.Severity;

public class Configuration
{
	private static final Map<String, JSONProperty<?>> sProperties = new HashMap<>();
	
	public static final Property<Integer> VERSION = register("version", new JSONProperty<>(Integer.valueOf(7)));
	public static final Property<Integer> INTERESTS = register("interests", new JSONProperty<>(Integer.valueOf(10)));
	public static final Property<Double> R211_DIFF = register("211-difference", new JSONProperty<>(Double.valueOf(0.1)));
	public static final Property<Double> R212_DIST = register("212-distance", new JSONProperty<>(Double.valueOf(0.2)));
	public static final Property<Double> RATIO = register("agent-edge-ratio", new JSONProperty<>(Double.valueOf(0.5)));
	public static final Property<Boolean> DISCONNECTABLE = register("can-split-graph", new JSONProperty<>(Boolean.valueOf(false)));
	public static final Property<Integer> MIN_CYCLES = register("runtime-floor", new JSONProperty<>(Integer.valueOf(100000)));
	public static final Property<Integer> MAX_CYCLES = register("runtime-ceiling", new JSONProperty<>(Integer.valueOf(1000000)));
	public static final Property<Boolean> STRICT = register("strict-mode", new JSONProperty<>(Boolean.valueOf(false)));
	
	public static final Property<Double> FACTOR_211 = register("chance-211", new JSONProperty<>(Double.valueOf(1.0)));
	public static final Property<Double> FACTOR_212 = register("chance-212", new JSONProperty<>(Double.valueOf(1.0)));
	public static final Property<Double> FACTOR_213 = register("ratio-213", new JSONProperty<>(Double.valueOf(5.0)));
	
	public static final Property<String[]> LOGGERS = register("loggers", new JSONProperty<>(new String[] {}));
	
	public static final Property<String> GENERATOR = register("generator", new JSONProperty<>("star-rivals-generator"));
	public static final Property<UniformGenerator> UNIGEN = register("uniform-generator", new JSONProperty<>(new UniformGenerator(7, 7, new UniformGenerator.RandomInitializer(0.2, 2)))); 
	public static final Property<StarRivalsGenerator> STARRIVGEN = register("star-rivals-generator", new JSONProperty<>(new StarRivalsGenerator(5, 3, 0.5, 10, 0)));
	public static final Property<TriangleGenerator> TRIGEN = register("triangle-generator", new JSONProperty<>(new TriangleGenerator(7, 0.5, 10, 0.0)));
	public static final Property<AsymmetryGenerator> ASYMGEM = register("asymmetry-generator", new JSONProperty<>(new AsymmetryGenerator(5, 5, 0.5, 3)));
	public static final Property<RandomGenerator> RANDGEN = register("random-generator", new JSONProperty<>(new RandomGenerator(16, 10, 0.2, 5)));
	
	public static final Property<String> WATCHER = register("watcher", new JSONProperty<>("empty-watcher"));
	public static final Property<EmptyWatcher> EMPTYWATCH = register("empty-watcher", new JSONProperty<>(new EmptyWatcher()));
	public static final Property<SingularityWatcher> SINGWATCH = register("singularity-watcher", new JSONProperty<>(new SingularityWatcher()));
	public static final Property<DeviationWatcher> DEVWATCH = register("deviation-watcher", new JSONProperty<>(new DeviationWatcher(0.0001)));
	public static final Property<RivalsOWWatcher> RIVALSOWWATCH = register("rivals-ow-watcher", new JSONProperty<>(new RivalsOWWatcher()));
	public static final Property<ChangeWatcher> CHANGEWATCH = register("change-watcher", new JSONProperty<>(new ChangeWatcher()));
	
	private static <T> JSONProperty<T> register(String name, JSONProperty<T> p)
	{
		sProperties.put(name, p);
		
		return p;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T get(String id)
	{
		return (T) sProperties.get(id).getValue();
	}
	
	public static JsonValue save( )
	{
		return sProperties.entrySet().stream().collect(
				JsonCollectors.ofObject((json, e) ->
					json.put(e.getKey(), e.getValue().save())));
	}
	
	public static void load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		if(!sProperties.keySet().containsAll(o.keySet()))
		{
			LOG.log(Severity.WARNING, "Loading incompatible config file!");
		}
		
		o.keySet().stream().forEach(id -> {
			JSONProperty<?> p = sProperties.get(id);
			
			p.load(CASUtils.merge(p.save(), o.get(id)));
		});
	}
	
	private static final Logger LOG = Logger.get("config");
}
