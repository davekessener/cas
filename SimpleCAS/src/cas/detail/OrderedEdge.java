package cas.detail;

public class OrderedEdge
{
	public final Entry bigger, smaller;
	public final double orientation;
	
	public OrderedEdge(Environment e, Relation r)
	{
		int ia = r.left;
		int ib = r.right;
		Agent aa = e.getAgent(ia);
		Agent ab = e.getAgent(ib);
		double va = e.value(aa);
		double vb = e.value(ab);
		
		Entry a = new Entry(ia, aa, va);
		Entry b = new Entry(ib, ab, vb);
		
		orientation = e.orientation(r);
		
		if(va < vb)
		{
			bigger = b;
			smaller = a;
		}
		else
		{
			bigger = a;
			smaller = b;
		}
	}

	public static final class Entry
	{
		public final int index;
		public final Agent agent;
		public final double value;
		
		public Entry(int index, Agent agent, double value)
		{
			this.index = index;
			this.agent = agent;
			this.value = value;
		}
	}
}
