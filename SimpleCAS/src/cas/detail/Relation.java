package cas.detail;

import dave.json.Container;
import dave.json.JsonBuilder;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;

@Container
public class Relation implements Saveable
{
	public final int left, right;
	
	public Relation(int l, int r)
	{
		if(l < 0 || r < 0 || l == r)
			throw new IllegalArgumentException();
		
		left = l < r ? l : r;
		right = l < r ? r : l;
	}
	
	public boolean includes(int i) { return left == i || right == i; }
	public int other(int v) { return left == v ? right : left; }
	
	@Override
	@Saver
	public JsonValue save( )
	{
		return (new JsonBuilder()).putInt("left", left).putInt("right", right).toJSON();
	}
	
	@Loader
	public static Relation load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		
		int l = o.getInt("left");
		int r = o.getInt("right");
		
		return new Relation(l, r);
	}
	
	@Override
	public int hashCode( )
	{
		return Integer.hashCode(left) ^ (Integer.hashCode(right) * 3);
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof Relation)
		{
			Relation r = (Relation) o;
			
			return left == r.left && right == r.right;
		}
		
		return false;
	}
}
