require_relative '../run'

Harness.start(graph: false) do |harness|
	harness.config['runtime-floor'] = 0
	harness.config['runtime-ceiling'] = 10000000
	harness.config['generator'] = 'uniform-generator'
	harness.config['watcher'] = 'singularity-watcher'
	harness.config['loggers'] = {
		'content' => [
			'average-deviation'
		]
	}
	harness.config['uniform-generator'] = {
		'content' => {
			'width' => 10,
			'height' => 10
		}
	}
	harness.config['deviation-watcher'] = {
		'content' => {
			'threshold' => 0.00001
		}
	}

    File.open('run.txt', 'w') do |f|
        1000.times do |i|
            r = harness.run
            
            if r =~ /after ([0-9]+) steps/
                c = $1.to_i
                f.write("#{i} #{c}\n")
                f.flush
            end
        end
    end
end
