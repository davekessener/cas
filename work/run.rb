require 'json'
require 'time'
require 'fileutils'
require 'tempfile'

require_relative 'graphify'

class Configuration
	def initialize
		@config = {}
		yield self if block_given?
	end

	def [](key)
		@config[key]
	end
	
	def []=(key, value)
		@config[key] = value
	end

    def load(c)
        c.each do |k, v|
            @config[k] = v
        end
    end

	def delete(key)
		@config.delete key
	end

	def generate
		JSON.pretty_generate(@config)
	end

	def write(fn)
		File.open(fn, 'w') do |f|
			f.write(generate)
		end
	end
end

class Harness
	attr_reader :config

	OPTIONS = {
		directory: '.',
		graph: true
	}

	def self.start(**opts)
		yield Harness.new(opts)
		puts "\a\a\a\a\a"
	end

	def initialize(**opts)
		opts = OPTIONS.merge(opts)
		
		@base = opts[:directory]
		@graph = opts[:graph]

		@log = []
		@config = Configuration.new
		@counter = 0
		@source = File.dirname(__FILE__)
	end

	def for(name, range)
		range.each do |v|
			@config[name] = v
			break unless yield v
		end
		@log
	end

	def run(*path)
		@counter += 1

		id = Time.now.to_s.split(/[ \t]+/)[0..1].join('').gsub(/[^0-9]/, '')
		id = "cas_#{id}_#{@counter}"
		
		d = "#{@base}/#{path.join('/')}/#{id}".gsub(/\/\/+/, '/')
		r = nil
		cfg_fn = "#{d}/config.json"
		out_fn = "#{d}/out.json"
		log_fn = "#{d}/out.log"

		FileUtils.mkdir_p(d)
		@config.write(cfg_fn)
		File.open("#{d}/log.txt", 'w') do |f|
			cmd = "java -jar \"#{@source}/CAS.jar\" --config \"#{cfg_fn}\" --output \"#{out_fn}\" --log \"#{log_fn}\""
			r = `#{cmd} 2>&1 | #{@source}/ssplit`

			f.write(r)
			@log.push(r)
		end

		Graphify.run(out_fn) if @graph
		
		r
	end
end

#if ARGV.length > 0
#	d = ARGV[0]
#	cfg = Configuration.new
#
#	cfg['runtime-floor'] = 0
#	cfg['runtime-floor'] = 0
#	cfg['runtime-ceiling'] = 5000000
#	cfg['generator'] = 'star-rivals-generator'
#	cfg['watcher'] = 'singularity-watcher'
#	cfg['211-difference'] = 0.05
#
#	File.open("#{d}/log.txt", 'a') do |log|
#		(30...100).each do |i|
#			(1..16).each do |j|
#				o = Harness.new("#{d}/n_#{'%02i' % j}")
#				cfg['star-rivals-generator'] = {
#					'content' => {
#						'core' => 0,
#						'edge' => j
#					}
#				}
#
#				r = o.run(cfg)
#
#				log.write("### i = #{i}, j = #{j}\n")
#				log.write("#{r}\n")
#				log.flush
#			end
#		end
#	end
#
#	File.open('counting.txt', 'w') do |f|
#		o = Harness.new("counting")
#		((0.01)..(0.15)).step(0.01).each do |chance|
#			cfg['211-difference'] = chance
#
#			a = 0
#			100.times do
#				(1..16).each do |i|
#					cfg['star-rivals-generator'] = {
#						'content' => {
#							'core' => 0,
#							'edge' => i
#						}
#					}
#
#					rr = o.run(cfg)
#
#					unless rr =~ /PIVOT/
#						f.write("#{chance}:#{i}")
#						f.flush
#						a += i
#						break
#					end
#				end
#			end
#
#			f.write("### #{chance} == #{a * 0.01}")
#			f.flush
#		end
#	end
#end


