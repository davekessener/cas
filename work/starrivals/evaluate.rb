require 'fileutils'
require 'chunky_png'

FileUtils.mkdir_p('out')

Dir.glob('cas_*/out.log').each do |fn|
	fn =~ /cas_[0-9]+_([0-9]+)\//
	i = $1.to_i
	c = File.read(fn).split("\n").map do |line|
		if line =~ /^@([0-9]+): rivals-ow = (-?[0-9]+(\.?[0-9]*)?)$/
			[$1.to_i, $2.to_f]
		end
	end

	w, h = 1000, 400
	img = ChunkyPNG::Image.new(w, h, ChunkyPNG::Color::WHITE)
	x = c.map { |x, y| x }
	y = c.map { |x, y| y }
	min, max = x.min, x.max
	x = x.map { |v| (v.to_f - min) / (max - min) }
	y = y.map { |v| (-v + 1) * 0.5 }

	img.line(0, h/2, w, h/2, 0x0000FFFF)

	to_x = ->(x) { (x * w).to_i }
	to_y = ->(y) { (y * h).to_i }
	draw = ->(x, y, color, th) {
		x2, y2 = to_x.(x[0]), to_y.(y[0])
		(x.length-1).times do |j|
			x1, y1 = x2, y2
			x2, y2 = to_x.(x[j+1]), to_y.(y[j+1])
			((-th/2)...((th+1)/2)).each do |dy|
				img.line(x1, y1 + dy, x2, y2 + dy, color)
			end
		end
	}

	sx, sy = [], []
	n = (c.length + 99) / 100
	100.times do |j|
		tx, ty = [], []
		n.times do |k|
			ii = j * n + k
			break if ii >= c.length
			tx.push(x[ii])
			ty.push(y[ii])
		end
		unless tx.empty? or ty.empty?
			sx.push(tx.sum.to_f / tx.length)
			sy.push(ty.sum.to_f / ty.length)
		end
	end

	draw.(x, y, 0x000000FF, 1)
	draw.(sx, sy, 0xFF0000FF, 3)

	img.save("out/ow_#{i}.png")

	puts "DONE #{i}"
end

