require_relative '../run'

Harness.start(graph: false) do |harness|
	harness.config['runtime-floor'] = 0
	harness.config['runtime-ceiling'] = 2000000
	harness.config['generator'] = 'star-rivals-generator'
	harness.config['watcher'] = 'singularity-watcher'
	harness.config['strict-mode'] = true
	harness.config['211-difference'] = 0.025
	harness.config['loggers'] = {
		'content' => [
#			'average-deviation',
			'rivals-ow'
		]
	}
	harness.config['star-rivals-generator'] = {
		'content' => {
			'core' => 0,
			'edge' => 7
		}
	}
	harness.config['uniform-generator'] = {
		'content' => {
			'width' => 10,
			'height' => 10
		}
	}
	harness.config['deviation-watcher'] = {
		'content' => {
			'threshold' => 0.00001
		}
	}

	100.times do |i|
		harness.run
	end
end

