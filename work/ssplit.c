#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    int c;

    while((c = fgetc(stdin)) >= 0)
    {
        fputc(c, stdout);
        fputc(c, stderr);
    }

    return EXIT_SUCCESS;
}
