require 'chunky_png'
require 'json'

class Vec2
	attr_reader :dx, :dy

	def initialize(dx, dy)
		@dx, @dy = dx.to_f, dy.to_f

		raise if @dx.nan? or @dy.nan?
	end

	def dot(v)
		@dx * v.dx + @dy * v.dy
	end

	def len_sqr
		dot(self)
	end

	def length
		@length ||= Math.sqrt(len_sqr)
	end

	def +(v)
		Vec2.new(@dx + v.dx, @dy + v.dy)
	end

	def -@
		Vec2.new(-@dx, -@dy)
	end

	def *(s)
		Vec2.new(s * @dx, s * @dy)
	end

	def -(v)
		self + (-v)
	end

	def /(s)
		self * (1.0 / s)
	end

	def normalize
		l = length
		(l == 0 ? Vec2.new(0, 0) : (self / l))
	end

	def to_s
		"(#{'%2s' % @dx.to_s} | #{'%2s' % @dy.to_s})"
	end
end

[Integer, Float].each do |c|
	c.class_eval do
		alias_method :_old_mul, :*

		def *(v)
			if v.is_a? Vec2
				v * self
			else
				_old_mul(v)
			end
		end
	end
end

class Array
	alias_method :_old_sum, :sum

	def sum
		if all? { |e| e.is_a? Vec2 }
			r = Vec2.new(0, 0)
			each { |e| r += e }
			r
		else
			_old_sum
		end
	end
end

module Graphify

	def self.out(v)
		STDERR.write(v)
		STDERR.flush
	end

class Node
	attr_reader :weight, :pos, :color

	def initialize(w, p, c)
		@weight, @pos, @color = w, p, c
	end

	def move(v)
		@pos += v
	end
end

class Edge
	attr_accessor :left, :right
	attr_accessor :weight

	def initialize(r)
		@left, @right = r['left'], r['right']
		@weight = r['weight']
	end
end

module Frame
	def self.included(base)
		base.send :include, InstanceMethods
		base.extend ClassMethods
	end

	module InstanceMethods
		def calculate(f)
			forces = []
			self.class.class_variable_get(:@@force_methods).each do |m|
				forces.push(send m)
			end
			forces = (0...@nodes.length).map { |i| forces.map { |a| a[i] }.sum }
			@nodes.each_with_index { |n, i| n.move(f * forces[i]) }
		end
	end

	module ClassMethods
		def add_force(name, &block)
			@@midx ||= :@@force_methods
			n = "_force_#{name}".to_sym
			define_method n, &block
			class_variable_set(@@midx, []) unless class_variables.include? @@midx
			class_variable_get(@@midx).push(n)
		end
	end
end

class Graph
	include Frame

	add_force :coulomb do
		(0...@nodes.length).map do |i|
			n = @nodes[i]

			(0...@nodes.length).map do |j|
				next if i == j

				o = @nodes[j]
				v = o.pos - n.pos
				d = v.length

				if d == 0
					puts "WARN: Node #{i} and #{j} occupy the same space!"
					o.move(Vec2.new(0.1, 0.1))
					nil
				else
					(3 + n.weight + o.weight) * v.normalize / (d * d)
				end
			end.select { |e| e.is_a? Vec2 }.sum
		end
	end

	add_force :springs do
		(0...@nodes.length).map do |i|
			n = @nodes[i]
			f = Vec2.new(0, 0)
			@edges.select { |e| e.left == i or e.right == i }.each do |e|
				j = (i == e.left ? e.right : e.left)
				o = @nodes[j]
				v = o.pos - n.pos
				d = v.length
				r = Vec2.new(0, 0)

				if d == 0
					puts "WARN: Node #{i} and #{j} occupy the same space!"
					o.move(Vec2.new(0.1, 0.1))
				else
					r = 0.085 * ((1 + e.weight.abs) * v.normalize * (d - (75 - 30 * e.weight)))
				end

				f += r
			end
			f
		end
	end

	add_force :gravity do
		@nodes.map { |n| 0.05 * -n.pos }
	end

	attr_reader :nodes, :edges

	def initialize(nodes, edges)
		@nodes, @edges = nodes, edges
	end
end

def self.generate(graph)
	agents = graph['nodes'].map { |n| n['value'] }
	colors = graph['nodes'].map { |n| ((n['tags'].find { |t| t.start_with? 'COLOR:#' } || 'COLOR:#FFFFFF')[7..-1] + 'FF').to_i(16) }
	rels = graph['edges']
	edges = (0...agents.length).map do |i|
		rels.map do |r|
			case i
				when r['left']
					r['right']
				when r['right']
					r['left']
				else
					nil
			end
		end.select do |v|
			not v.nil?
		end
	end
	idx = (0...agents.length).to_a.sort { |a, b| edges[b].length - edges[a].length }
	nodes = Array.new(agents.length)

	first = idx[0]
	nodes[first] = Node.new(agents[first], Vec2.new(0, 0), colors[first])
	open = [[first, 0.0, 1000]]

	until open.empty?
		i, d, l = *open.shift
		n = nodes[i]
		p = n.pos
		new = edges[i].select { |r| nodes[r].nil? }
		new.each_with_index do |o, j|
			f = rand * 0.3 * 2 * Math::PI / new.length
			f += d + 2 * Math::PI * j / new.length
			v = p + l * Vec2.new(Math.sin(f), Math.cos(f))
			nodes[o] = Node.new(agents[o], v, colors[o])
			open.push([o, f, l - 10])
		end
	end

	unknown = []
	nodes.each_with_index do |node, i|
		unknown.push(i) if node.nil?
	end

	puts "Nodes #{unknown.map(&:to_s).join(', ')} seem to be not connected!" unless unknown.empty?

	rels.map! { |r| Edge.new(r) }
	until unknown.empty?
		i = unknown.pop
		rels.each do |r|
			raise "#{i} appeared in edge but not node-list!" if r.left == i or r.right == i
			r.left -= 1 if r.left > i
			r.right -= 1 if r.right > i
		end
	end
	nodes.reject! { |n| n.nil? }

	raise if rels.any? { |r| r.left == r.right }

	Graph.new(nodes, rels)
end

def self.process(graph, n = 750, f = 0.01)
	p = n / 100
	out("\b\b\b")
	n.times do |i|
		if (i % p) == 0
			s = ('%4s' % "#{i * 100 / n}%")
			out("#{s}#{"\b" * s.length}")
		end
		graph.calculate(f)
	end
	out("...  \b\b")
	graph
end

def self.write(graph, l = 1000)
	center = Vec2.new(0, 0)
	graph.nodes.each { |n| center += n.pos }
	center /= graph.nodes.length
	scale = graph.nodes.map { |n| (center - n.pos).length }.max
	offset = Vec2.new(1, 1)
	smallest, largest = -1, -1
	graph.nodes.map { |n| n.weight.to_f }.tap { |a| smallest, largest = a.min, a.max }

	if scale > 0.0
		scale = 0.9 / scale
	else
		puts "Max distance is #{scale}"
		scale = 1.0
	end

	nodes = graph.nodes.map { |n| [n.weight, l * 0.5 * ((n.pos - center) * scale + offset), n.color] }
	edges = graph.edges.map { |e| [e.weight, nodes[e.left][1], nodes[e.right][1]] }

	image = ChunkyPNG::Image.new(l, l, ChunkyPNG::Color::WHITE)
	edges.each do |weight, left, right|
		c = (255 * weight.abs).to_i
		c = (c << (weight < 0 ? 24 : 16)) | 0xFF
		image.line(left.dx.to_i, left.dy.to_i, right.dx.to_i, right.dy.to_i, c, 2)
	end
	nodes.each do |weight, pos, color|
		s = 5 + 10 * ((weight - smallest) / (largest - smallest))
		image.circle(pos.dx.to_i, pos.dy.to_i, s.to_i, ChunkyPNG::Color::BLACK, color)
	end

	image
end

def self.run(in_fn)
	out_fn = in_fn.sub(/\.[^.]+$/, '.png')

	out("loading graph from #{in_fn} ...")
	graph = JSON.parse(File.read(in_fn))['graph']
	out("\b\b\b[DONE]\n")

	out("generating initial layout ...")
	graph = Graphify.generate(graph)
	out("\b\b\b[DONE]\n")

#	write(graph).save(in_fn.sub(/\.[^.]+$/, '_raw.png'))

	out("Running simulation ...")
	graph = Graphify.process(graph)
	out("\b\b\b[DONE]\n")

	out("Generating output image ...")
	image = Graphify.write(graph)
	out("\b\b\b[DONE]\n")

	out("Writing image to file #{out_fn} ...")
	image.save(out_fn)
	out("\b\b\b[DONE]\n")
end
end

if __FILE__ == $0
	Graphify.run(ARGV[0])
end

