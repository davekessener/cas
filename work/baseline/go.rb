require_relative '../run'

Harness.start(graph: false) do |harness|
	harness.config['runtime-floor'] = 0
	harness.config['runtime-ceiling'] = 2000000
	harness.config['watcher'] = 'singularity-watcher'
	harness.config['uniform-generator'] = {
		'content' => {
			'width' => 4,
			'height' => 4
		}
	}
	harness.config['star-rivals-generator'] = {
		'content' => {
			'core' => 0,
			'edge' => 7
		}
	}
	harness.config['deviation-watcher'] = {
		'content' => {
			'threshold' => 0.00001
		}
	}

	result = {}
	['star-rivals-generator', 'uniform-generator'].each do |gen|
		harness.config['generator'] = gen

		total = count = 0
		1000.times do |i|
			puts "#{gen}: #{i} (#{'%.2f' % (total / count.to_f)})"

			r = harness.run(gen)

			if r =~ /after ([0-9]+) steps/
				total += $1.to_i
				count += 1
			end
		end

		result[gen] = total / count.to_f
	end

	result.each do |k, v|
		puts "#{k}: #{v}"
	end
end

