module EmbeddedRuby
    def self.process(raw, **locals)
        raw.gsub(/[\n\r]+/, "\n").gsub(/\t+/, ' ').gsub(/<!--#(.*?)-->/m) do
            code = $1
            env = Object.new
            locals.each do |k, v|
                env.instance_variable_set "@#{k}".to_sym, v
            end
            env.instance_eval code
        end
    end
end
