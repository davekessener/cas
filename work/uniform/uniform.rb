require_relative '../run'

Harness.start(graph: false) do |harness|
	harness.config['runtime-floor'] = 0
	harness.config['runtime-ceiling'] = 2000000
	harness.config['generator'] = 'uniform-generator'
	harness.config['watcher'] = 'singularity-watcher'
	harness.config['loggers'] = {
		'content' => [
			'average-deviation'
		]
	}
	harness.config['uniform-generator'] = {
		'content' => {
			'width' => 10,
			'height' => 10
		}
	}
	harness.config['deviation-watcher'] = {
		'content' => {
			'threshold' => 0.00001
		}
	}

	100.times do |i|
		harness.run("run_s_#{i}")
	end
end

