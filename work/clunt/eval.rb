class Entry
	attr_reader :index, :size, :pivot, :total

	def initialize(i, j)
		@index = i
		@size = j
	end

	def valid?
		(!!@pivot) && (!!@total)
	end

	def process(line)
		if line =~ /PIVOT @([0-9]+) \(([01](\.[0-9]*))\)/
			@pivot = $2.to_f
		elsif line =~ /TOTAL @([0-9]+) in ([0-9]+)/
			@total = $2.to_i
		end
	end
end

content = []
File.open('log.txt', 'r') do |f|
	while (line = f.gets)
		if line =~ /^### i = ([0-9]+), j = ([0-9]+)/
			content.push(Entry.new($1.to_i, $2.to_i))
		elsif not content.empty?
			content.last.process(line)
		end
	end
end

content.reject! { |e| not e.valid? }

16.times do |i|
	all = content.select { |e| e.size == (i + 1) }
	pivot = all.map { |e| e.pivot * e.total }.sum / all.length
	total = all.map { |e| e.total.to_f }.sum / all.length

	puts "#{i + 1} #{'%.3f' % total};"
end

