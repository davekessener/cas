require 'socket'

require_relative 'erb'

if ARGV.length < 2
    puts "usage: ruby #{$0} HOST PORT"
    exit(1)
end

HOST = ARGV[0]
PORT = ARGV[1].to_i

webserver = TCPServer.new(HOST, PORT)

puts "Starting server on [#{HOST}]:#{PORT}"

while (session = webserver.accept)
    begin
        request = session.gets

        puts "received #{request}"

        loop do
            break if (header = "#{session.gets}".strip).empty?

            puts header
        end

        if request =~ /GET \/([a-zA-Z0-9\/_%$.+-]*)/
            fn = $1

            fn = fn[0...(fn.length-1)] if fn.end_with? '/'

            if fn.empty? or File.directory? fn
                puts "[OK] sending directory #{fn}"
                
                if File.exists? "#{fn}/index.html"
                    session.print "HTTP/1.1 200 OK\r\n\r\n"
                    session.print File.read("#{fn}/index.html")
                elsif File.exists? "#{fn}/index.erb"
                    begin
                        r = EmbeddedRuby::process(File.read("#{fn}/index.erb"))
                        session.print "HTTP/1.1 200 OK\r\n\r\n"
                        session.print r
                    rescue
                        session.print "HTTP/1.1 500\r\n\r\n"
                    end
                else
                    session.print "HTTP/1.1 200 OK\r\n\r\n"
                    session.print EmbeddedRuby::process(File.read("index.erb"), dir: fn)
                end
            elsif File.exists? fn
                puts "[OK] sending '#{fn}'"

                if fn.end_with? '.erb'
                    begin
                        r = EmbeddedRuby::process(File.read(fn))
                        session.print "HTTP/1.1 200 OK\r\n\r\n"
                        session.print r
                    rescue
                        session.print "HTTP/1.1 500\r\n\r\n"
                    end
                else
                    session.print "HTTP/1.1 200 OK\r\n\r\n"
                    session.print File.read(fn)
                end
            else
                puts "[ERR] unknown file '#{fn}'!"

                session.print "HTTP/1.1 404\r\n\r\n"
            end
        else
            puts "[ERR] invalid request!"

            session.print "HTTP/1.1 400\r\n\r\n"
        end

        puts "[CLOSING]"
        
        session.close
    rescue Errno::ECONNRESET
        session = webserver.accept
    end
end