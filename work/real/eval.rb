failed = []
Dir.glob("n_*").each do |d|
	File.open("#{d}.txt", 'w') do |f|
		Dir.glob("#{d}/cas_*/log.txt").each do |fn|
			if File.read(fn) =~ /after ([0-9]+) steps/
				c = $1.to_i
				f.write("#{c}\n")
				failed.push(d) if c >= 15000000
			end
		end
	end
end

failed.uniq.each { |d| puts "FAILED #{d}!" }

