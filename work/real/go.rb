require '../run'

class Averager
    def initialize
        @total, @count = 0, 0
    end

    def value
        (@count > 0 ? (@toal / @count.to_f) : 0)
    end

    def add(v)
        @total += v
        @count += 1
    end
end

n = 24
Harness.start(graph: false) do |harness|
    r = Array.new(n) { Averager.new }
    1000.times do |i|
        n.times do |j|
            harness.config.load({
                'runtime-floor' => 0,
                'runtime-ceiling' => 15000000,
                '211-difference' => 0.1,
                'generator' => 'star-rivals-generator',
                'watcher' => 'singularity-watcher',
                'star-rivals-generator' => {
                    'content' => {
                        'core' => 0,
                        'edge' => (j+1),
                        'retention' => 0.5,
                        'scale' => 10,
                        'cross' => 0
                    }
                }
            })

            puts "!!! n=#{j+1}, i=#{i}"

            v = harness.run("n_#{'%02i' % j}")

            if v =~ /after ([0-9]+) steps/
                c = $1.to_i

                if c < 15000000
                    r[j].add(c)
                else
                    `touch "n#{j}_FAILED.txt"`
                end
            end
        end
    end
    r.each_with_index { |a, i| puts "#{i+1}: #{a.value}" }
end
