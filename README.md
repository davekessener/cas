# Complex Adaptive Systems
### HAW Hamburg (WS 18/19)

Simulation environment for a simple multi-agent system.
Visualized by representing agents as nodes in a graph.
Utilizes force-directed graph drawing.
Written in Java.

