package cas.app;

import java.util.concurrent.ExecutionException;

import cas.app.components.CameraComponent;
import cas.app.components.CommandComponent;
import cas.app.components.CompassComponent;
import cas.app.components.GeometryComponent;
import cas.app.components.KeyboardComponent;
import cas.app.components.SimulationComponent;
import cas.app.components.TimerComponent;
import cas.app.control.Key;
import cas.app.control.Keyboard;
import cas.app.control.MouseCapture;
import cas.model.Environment;
import cas.model.rules.Rule_211;
import cas.model.rules.Rule_212;
import cas.model.rules.Rule_221;
import cas.model.rules.Rule_222;
import cas.model.rules.Rule_223;
import cas.sim.Entity;
import cas.sim.World;
import cas.sim.entity.ModelRenderingModule;
import cas.sim.model.NodeModel;
import cas.sim.world.CoulombPlugin;
import cas.sim.world.EntityPlugin;
import cas.sim.world.GravityPlugin;
import cas.sim.world.RenderPlugin;
import cas.util.Utils;
import cas.util.geo.Vec3;
import cas.util.geo.fx.FxCamera;
import cas.util.props.Properties;
import cas.util.shell.AliasCommand;
import cas.util.shell.ShellClient;
import cas.util.shell.SimpleCommand;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Duration;

public abstract class BaseApp extends AbstractApp
{
	private final Stage mPrimary;
	private final FxCamera mCamera;
	private final World mWorld;
	private final Environment mEnvironment;
	private final Keyboard mKeyboard;
	private final ShellClient mShell;
	private final TimerComponent mTimer;
	private final SimulationComponent mSimulation;
	
	public BaseApp(Stage primary)
	{
		super(FPS);
		
		mPrimary = primary;
		mCamera = new FxCamera();
		mWorld = new World();
		mEnvironment = new Environment(INTERESTS);
		mKeyboard = new Keyboard();
		mShell = new ShellClient();
		mTimer = new TimerComponent();
		mSimulation = new SimulationComponent(mEnvironment, mWorld);
		
		new RenderPlugin(mWorld);
		new GravityPlugin(Vec3.ORIGIN, 5, mWorld);
		new CoulombPlugin(5, mWorld);
		new EntityPlugin(mWorld);

		KeyboardComponent kb = new KeyboardComponent(mKeyboard);
		CameraComponent camera = new CameraComponent(mCamera, mKeyboard);
		MouseCapture cursor = new MouseCapture(mPrimary, camera::update);
		GeometryComponent geo = new GeometryComponent(mWorld, camera.getCamera());
		
		AnchorPane root = new AnchorPane();
		
		mKeyboard.onKeyPressed(Key.ESCAPE, key -> shutdown());
		
		mShell.add(new AliasCommand(new SimpleCommand("help", "help: prints all commands", this::help), "h"));
		mShell.add(new AliasCommand(new SimpleCommand("shutdown", "shutdown: terminates program", this::shutdown), "q", "quit", "exit"));
		mShell.add(new SimpleCommand("move-to", "move-to X Y Z: moves camera to XYZ", this::moveTo));
		mShell.add(new SimpleCommand("look-at", "look-at X Y Z: makes camera face towards XYZ", this::lookAt));
		mShell.add(new SimpleCommand("camera-pos", "camera-pos: prints current camera position", this::cameraPos));

		kb.add(KeyCode.W, Key.FORWARD);
		kb.add(KeyCode.S, Key.BACKWARD);
		kb.add(KeyCode.A, Key.LEFT);
		kb.add(KeyCode.D, Key.RIGHT);
		kb.add(KeyCode.SPACE, Key.UP);
		kb.add(KeyCode.SHIFT, Key.DOWN);
		
		kb.add(KeyCode.UP, Key.DOWN);
		kb.add(KeyCode.DOWN, Key.UP);
		kb.add(KeyCode.LEFT, Key.RIGHT);
		kb.add(KeyCode.RIGHT, Key.LEFT);

		kb.add(KeyCode.ESCAPE, Key.ESCAPE);

		add(geo);
		add(camera);
		add(kb);
		add(new CommandComponent(mShell));
		add(new CompassComponent());
		add(mSimulation);
		add(mTimer);
		
		root.getChildren().addAll(super.getUI());
		
		Scene scene = new Scene(root, SCREEN_WIDTH, SCREEN_HEIGHT);
		
		scene.setOnMousePressed(cursor);
		scene.setOnMouseReleased(cursor);
		scene.setOnMouseDragged(cursor);
		scene.setOnKeyPressed(kb);
		scene.setOnKeyReleased(kb);
		
		geo.bind(scene);
		
		mEnvironment.add(new Rule_211());
		mEnvironment.add(new Rule_212());
		mEnvironment.add(new Rule_221());
		mEnvironment.add(new Rule_222());
		mEnvironment.add(new Rule_223());
		
		mPrimary.titleProperty().bind(Utils.conversionProperty(
			Properties.<Vec3>get(Properties.PLAYER_POS),
			v -> String.format("%s v%s - @(%.2f | %.2f | %.2f)", TITLE, VERSION, v.X, v.Y, v.Z)));
		mPrimary.setScene(scene);
		
		start();
		
		mPrimary.show();
		
		Entity origin = new Entity("origin", Vec3.ORIGIN, mWorld);
		NodeModel origin_model = new NodeModel();
		origin_model.setSize(-0.5);
		origin_model.setColor(Color.ORANGE);
		new ModelRenderingModule(origin_model, origin);
		
		mCamera.move(0, 0, -2500);
	}
	
	protected Environment environment( ) { return mEnvironment; }
	protected TimerComponent timer( ) { return mTimer; }
	protected SimulationComponent simulation( ) { return mSimulation; }
	
	private void help( )
	{
		mShell.commands().forEach(cmd -> System.out.println(cmd.help()));
	}

	private void shutdown( )
	{
		System.out.println("Sent shutdown request ...");
		
		mPrimary.fireEvent(new WindowEvent(mPrimary, WindowEvent.WINDOW_CLOSE_REQUEST));
	}

	private void moveTo(String[] args) throws ExecutionException
	{
		mCamera.moveTo(argVec(args));
	}

	private void lookAt(String[] args) throws ExecutionException
	{
		mCamera.lookAt(argVec(args));
	}
	
	private void cameraPos( )
	{
		Vec3 p = mCamera.getPosition();
		
		System.out.println(String.format("@(%.3f | %.3f | %.3f)", p.X, p.Y, p.Z));
	}
	
	private Vec3 argVec(String[] args) throws ExecutionException
	{
		double x = 0, y = 0, z = 0;
		
		if(args.length != 3)
		{
			throw new ExecutionException(new IllegalArgumentException("look-at X Y Z"));
		}
		
		try
		{
			x = Double.parseDouble(args[0]);
			y = Double.parseDouble(args[1]);
			z = Double.parseDouble(args[2]);
		}
		catch(NumberFormatException e)
		{
			throw new ExecutionException(e);
		}
		
		return new Vec3(x, y, z);
	}
	
	private static final String TITLE = "Complex Adaptive Systems";
	private static final int VERSION = 2;
	private static final int INTERESTS = 20;
	private static final int SCREEN_WIDTH = 640, SCREEN_HEIGHT = 480;
	private static final Duration FPS = Duration.seconds(1.0 / 30);
}
