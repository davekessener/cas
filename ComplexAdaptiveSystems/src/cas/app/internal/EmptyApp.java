package cas.app.internal;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import cas.app.BaseApp;
import cas.model.Actor;
import cas.model.Relation;
import cas.util.geo.Vec3;
import dave.json.JsonArray;
import dave.json.JsonObject;
import javafx.stage.Stage;

public class EmptyApp extends BaseApp
{
	private static interface IDGen { String gen(int x, int y, int z); }
	
	public EmptyApp(Stage primary)
	{
		super(primary);
		
		int s = 2, d = 250;
		IDGen getID = (x, y, z) -> String.format("node_%d_%d_%d", x+s, y+s, z+s);
		
		Vec3[] dirs = new Vec3[] { new Vec3(-1, 0, 0), new Vec3(0, -1, 0), new Vec3(0, 0, -1) };
		
		Map<String, Actor> lut = new HashMap<>();
		JsonArray nodes = new JsonArray();
		JsonArray springs = new JsonArray();
		
		for(int z = -s ; z <= s ; ++z)
		{
			for(int y = -s ; y <= s ; ++y)
			{
				for(int x = -s ; x <= s ; ++x)
				{
					Vec3 p = new Vec3(x, y, z);
					Actor a = environment().create();
					
					{
						String id = getID.gen(x, y, z);
						
						for(int i = 0 ; i < a.size() ; ++i)
						{
							if(Math.random() < 0.67)
							{
								a.set(i, (Math.random() - 0.5) * 5);
							}
						}
						
						lut.put(id, a);
					
						JsonObject json = new JsonObject();
						
						json.putInt("actor", environment().actors().indexOf(a));
						json.putString("id", id);
						json.putInt("count", 6);
						json.put("position", p.scale(d).save());
						
						nodes.add(json);
					}
					
					Stream.of(dirs).map(v -> p.add(v)).forEach(v -> {
						String id = getID.gen((int) v.X, (int) v.Y, (int) v.Z);
						Actor b = lut.get(id);
						
						if(b != null)
						{
							Relation r = environment().connect(a, b);
							
							JsonObject json = new JsonObject();
							
							json.putInt("relation", environment().relations().indexOf(r));
							json.putString("id", id);
							json.putInt("count", 2);
							json.put("position", p.scale(0.5).add(v.scale(0.5)).scale(d).save());
							
							springs.add(json);
						}
					});
				}
			}
		}
		
		JsonObject json = new JsonObject();
		
		json.put("environment", environment().save());
		json.put("nodes", nodes);
		json.put("springs", springs);
		
		simulation().load(json);
	}
}
