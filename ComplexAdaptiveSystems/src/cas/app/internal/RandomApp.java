package cas.app.internal;

import cas.app.BaseApp;
import cas.model.Actor;
import cas.model.Relation;
import cas.util.Utils;
import javafx.stage.Stage;

public class RandomApp extends BaseApp
{
	public RandomApp(Stage primary)
	{
		super(primary);

		environment().create();
		
		timer().schedule(0, 3, () -> {
			Actor b = environment().actors().get((int) (Math.random() * environment().actors().size()));
			Actor a = environment().create();
			
			for(int i = 0 ; i < a.size() ; ++i)
			{
				if(Math.random() < 0.35)
				{
					a.set(i, 5 * (Math.random() - 0.5));
				}
			}
			
			System.out.println("New actor: " + a.spectrum());
			
			if(a != b)
			{
				Relation r = environment().connect(a, b);
				
				System.out.println("Connected: " + r.orientation());
				System.out.println("Other has value of " + environment().valueOf(b));
			}
		});
		
		timer().schedule(20, 5, () -> {
			Actor a = Utils.any(environment().actors());
			Actor b = Utils.any(environment().actors());
			
			if(a != b)
			{
				environment().connect(a, b);
			}
		});
	}
}
