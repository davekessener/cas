package cas.app.common;

import javafx.scene.Node;

public interface UI
{
	public abstract Node getUI( );
}
