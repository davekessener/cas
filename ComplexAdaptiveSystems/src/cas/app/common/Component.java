package cas.app.common;

public interface Component
{
	public default void start( ) { }
	public default void stop( ) { }
}
