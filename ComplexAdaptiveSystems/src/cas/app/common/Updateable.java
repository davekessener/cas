package cas.app.common;

public interface Updateable
{
	public abstract void update(double delta);
}
