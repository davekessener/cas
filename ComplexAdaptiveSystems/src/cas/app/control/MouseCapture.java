package cas.app.control;

import java.awt.AWTException;
import java.awt.Robot;

import cas.util.SevereException;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class MouseCapture implements EventHandler<MouseEvent>
{
	private final Stage mStage;
	private final Robot mMover;
	private final MouseTarget mCallback;
	private boolean mDown, mSkip, mEnabled;
	
	public static interface MouseTarget { void move(double dx, double dy); }
	
	public MouseCapture(Stage s, MouseTarget f)
	{
		mStage = s;
		mCallback = f;
		mDown = false;
		mSkip = false;
		mEnabled = true;
		
		try
		{
			mMover = new Robot();
		}
		catch (AWTException e)
		{
			throw new SevereException(e);
		}
	}
	
	public void enable(boolean f)
	{
		mEnabled = f;
		
		if(!mEnabled && mDown)
		{
			onUp();
		}
	}

	@Override
	public void handle(MouseEvent e)
	{
		if(!mEnabled) return;
		
		if(e.getEventType() == MouseEvent.MOUSE_PRESSED)
		{
			if(!mDown && e.getButton() == MouseButton.PRIMARY)
			{
				onDown();
			}
		}
		else if(e.getEventType() == MouseEvent.MOUSE_RELEASED)
		{
			if(mDown && e.getButton() == MouseButton.PRIMARY)
			{
				onUp();
			}
		}
		else if(e.getEventType() == MouseEvent.MOUSE_DRAGGED)
		{
			if(mDown)
			{
				onMove(e);
			}
		}
	}
	
	private void onDown( )
	{
		mDown = true;
		mSkip = true;
		
		toCenter();
		
		mStage.getScene().setCursor(Cursor.NONE);
	}
	
	private void onUp( )
	{
		mDown = false;
		mSkip = false;
		
		mStage.getScene().setCursor(Cursor.DEFAULT);
	}
	
	private void onMove(MouseEvent e)
	{
		mSkip = !mSkip;
		
		if(mSkip)
		{
		    double dx =   e.getScreenX() - (mStage.getX() + (mStage.getWidth()  / 2.0)) ;
		    double dy = -(e.getScreenY() - (mStage.getY() + (mStage.getHeight() / 2.0)));
			
			toCenter();
			
			mCallback.move(dx, dy);
		}
	}
	
	private void toCenter( )
	{
		mMover.mouseMove((int) (mStage.getX() + (mStage.getWidth() / 2.0)), (int) (mStage.getY() + (mStage.getHeight() / 2.0)));
	}
}
