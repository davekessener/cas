package cas.app.control;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class Keyboard
{
	private final Map<Key, List<Consumer<Key>>> mCBPressed = new HashMap<>();
	private final Map<Key, List<Consumer<Key>>> mCBReleased = new HashMap<>();
	private boolean[] mKeys = new boolean[Key.values().length];
	
	public void onKeyPressed(Key key, Consumer<Key> f)
	{
		getList(mCBPressed, key).add(f);
	}
	
	public void onKeyReleased(Key key, Consumer<Key> f)
	{
		getList(mCBReleased, key).add(f);
	}
	
	public boolean isPressed(Key key)
	{
		return mKeys[key.ordinal()];
	}
	
	public void pressKey(Key key)
	{
		if(!isPressed(key))
		{
			mKeys[key.ordinal()] = true;
			
			for(Consumer<Key> cb : getList(mCBPressed, key))
			{
				cb.accept(key);
			}
		}
	}
	
	public void releaseKey(Key key)
	{
		if(isPressed(key))
		{
			mKeys[key.ordinal()] = false;
			
			for(Consumer<Key> cb : getList(mCBReleased, key))
			{
				cb.accept(key);
			}
		}
	}
	
	private List<Consumer<Key>> getList(Map<Key, List<Consumer<Key>>> map, Key key)
	{
		List<Consumer<Key>> list = map.get(key);

		if(list == null)
		{
			map.put(key, list = new ArrayList<>());
		}
		
		return list;
	}
}
