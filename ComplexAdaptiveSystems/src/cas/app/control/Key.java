package cas.app.control;

public enum Key
{
	FORWARD,
	BACKWARD,
	UP,
	DOWN,
	LEFT,
	RIGHT,
	ESCAPE
}
