package cas.app;

import java.util.ArrayList;
import java.util.List;

import cas.app.common.App;
import cas.app.common.Component;
import cas.app.common.UI;
import cas.app.common.Updateable;
import javafx.scene.Node;
import javafx.util.Duration;

public class AbstractApp implements App
{
	private final List<Component> mComponents;
	private final List<Node> mScene;
	private final Loop mLoop;
	
	public AbstractApp(Duration d)
	{
		mComponents = new ArrayList<>();
		mScene = new ArrayList<>();
		mLoop = new Loop(d, delta -> update(delta));
	}
	
	@Override
	public void start( )
	{
		for(Component c : mComponents)
		{
			c.start();
		}
		
		mLoop.start();
	}

	@Override
	public void stop( )
	{
		mLoop.stop();
		
		for(Component c : mComponents)
		{
			c.stop();
		}
	}
	
	public void add(Component c)
	{
		mComponents.add(c);
		
		if(c instanceof UI)
		{
			mScene.add(((UI) c).getUI());
		}
	}
	
	public List<Node> getUI( )
	{
		return mScene;
	}
	
	protected void update(double delta)
	{
		for(Component c : mComponents)
		{
			if(c instanceof Updateable)
			{
				((Updateable) c).update(delta);
			}
		}
	}
}
