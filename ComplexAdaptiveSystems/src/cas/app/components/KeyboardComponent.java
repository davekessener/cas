package cas.app.components;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import cas.app.common.Component;
import cas.app.common.Updateable;
import cas.app.control.Key;
import cas.app.control.Keyboard;
import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class KeyboardComponent implements Component, Updateable, EventHandler<KeyEvent>
{
	private final Keyboard mKeyboard;
	private final Queue<KeyEvent> mEvents;
	private final Map<KeyCode, Key> mTranslator;
	
	public KeyboardComponent(Keyboard kb)
	{
		mKeyboard = kb;
		mEvents = new ConcurrentLinkedQueue<>();
		mTranslator = new HashMap<>();
	}
	
	public KeyboardComponent add(KeyCode c, Key k)
	{
		mTranslator.put(c, k);
		
		return this;
	}
	
	@Override
	public void update(double delta)
	{
		for(KeyEvent e = null ; (e = mEvents.poll()) != null ;)
		{
			Key k = mTranslator.get(e.getCode());
			
			if(k != null)
			{
				if(e.getEventType() == KeyEvent.KEY_PRESSED)
				{
					mKeyboard.pressKey(k);
				}
				else if(e.getEventType() == KeyEvent.KEY_RELEASED)
				{
					mKeyboard.releaseKey(k);
				}
				else
				{
					System.err.println("Unknown KeyEvent: " + e + " (" + e.getEventType() + ")");
				}
			}
		}
	}

	@Override
	public void handle(KeyEvent e)
	{
		mEvents.add(e);
	}
}
