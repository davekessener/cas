package cas.app.components;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

import cas.app.common.Component;
import cas.app.common.Updateable;
import cas.util.InterruptibleInput;

public class CommandComponent implements Component, Updateable
{
	private final ExecutorService mAsync;
	private final Consumer<String> mCallback;
	private final Queue<String> mBuffer;
	private final InterruptibleInput mReader;
	private boolean mRunning;
	
	public CommandComponent(Consumer<String> f)
	{
		mAsync = Executors.newSingleThreadExecutor();
		mCallback = f;
		mBuffer = new ConcurrentLinkedQueue<>();
		mReader = new InterruptibleInput(new InputStreamReader(System.in));
		mRunning = false;
	}
	
	@Override
	public void start( )
	{
		System.out.print("> ");
		
		mRunning = true;
		mAsync.submit(this::run);
	}
	
	@Override
	public void stop( )
	{
		mRunning = false;
		mReader.close();
		mAsync.shutdownNow();
	}
	
	private void run( )
	{
		try
		{
			while(mRunning)
			{
				mBuffer.add(mReader.nextLine());
			}
		}
		catch(InterruptedException | IOException e)
		{
		}
	}

	@Override
	public void update(double delta)
	{
		boolean needs_prompt = false;
		
		for(String line = null ; (line = mBuffer.poll()) != null ; needs_prompt = true)
		{
			mCallback.accept(line);
		}

		if(mRunning && needs_prompt)
		{
			System.out.print("> ");
		}
	}
}
