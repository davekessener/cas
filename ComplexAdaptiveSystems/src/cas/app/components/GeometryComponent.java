package cas.app.components;

import cas.app.common.Component;
import cas.app.common.UI;
import cas.app.common.Updateable;
import cas.sim.World;
import cas.sim.common.Plugins;
import cas.sim.world.RenderPlugin;
import javafx.scene.Camera;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.SceneAntialiasing;
import javafx.scene.SubScene;
import javafx.scene.paint.Color;

public class GeometryComponent implements Component, Updateable, UI
{
	private final World mWorld;
	private final SubScene mScene;
	
	public GeometryComponent(World w, Camera c)
	{
		mWorld = w;
		
		RenderPlugin r = mWorld.getPlugin(Plugins.RENDER);
		
		mScene = new SubScene(r.getScene(), 150, 100, true, SceneAntialiasing.BALANCED);

		mScene.setFill(Color.BLACK);
		mScene.setCamera(c);
		mScene.setManaged(false);
		mScene.requestFocus();
	}
	
	public void bind(Scene s)
	{
		mScene.widthProperty().bind(s.widthProperty());
		mScene.heightProperty().bind(s.heightProperty());
	}

	@Override
	public Node getUI()
	{
		return mScene;
	}

	@Override
	public void update(double delta)
	{
		mWorld.update(delta);
	}
}
