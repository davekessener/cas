package cas.app.components;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import cas.app.common.Component;
import cas.app.common.Updateable;

public class TimerComponent implements Component, Updateable
{
	private final List<Task> mTasks = new LinkedList<>();
	
	public void schedule(double o, double p, Runnable f)
	{
		mTasks.add(new Task(o, p, f));
	}

	@Override
	public void update(double delta)
	{
		for(Iterator<Task> i = mTasks.iterator() ; i.hasNext() ;)
		{
			Task t = i.next();
			
			if((t.left -= delta) <= 0)
			{
				t.callback.run();
				
				if(t.period > 0)
				{
					t.left += t.period;
				}
				else
				{
					i.remove();
				}
			}
		}
	}

	private static final class Task
	{
		public double left;
		public final double period;
		public final Runnable callback;
		
		public Task(double o, double p, Runnable f)
		{
			left = o;
			period = p;
			callback = f;
		}
	}
}
