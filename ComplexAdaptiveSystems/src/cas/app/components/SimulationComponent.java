package cas.app.components;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import cas.app.common.Component;
import cas.app.common.Updateable;
import cas.model.Actor;
import cas.model.Environment;
import cas.model.Relation;
import cas.sim.Entity;
import cas.sim.World;
import cas.sim.model.Node;
import cas.sim.model.Spring;
import cas.util.geo.Vec3;
import cas.util.props.Properties;
import dave.json.JsonCollectors;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loadable;
import dave.json.Saveable;
import javafx.beans.property.Property;

public class SimulationComponent implements Component, Updateable, Saveable, Loadable
{
	private static final Property<Number> SIM_SPEED = Properties.get(Properties.SIM_SPEED);
	
	private final Environment mEnv;
	private final World mWorld;
	private final Map<Actor, Entry<Node>> mActors;
	private final Map<Relation, Entry<Spring>> mRelations;
	private int mCounter;
	
	public SimulationComponent(Environment e, World w)
	{
		mEnv = e;
		mWorld = w;
		mActors = new HashMap<>();
		mRelations = new HashMap<>();
		mCounter = 0;
	}
	
	@Override
	public JsonValue save( )
	{
		JsonObject json = new JsonObject();
		
		json.put("environment", mEnv.save());
		json.put("nodes", mActors.entrySet().stream().map(e -> {
			JsonObject o = (JsonObject) e.getValue().save();

			o.putInt("actor", mEnv.actors().indexOf(e.getKey()));
			
			return o;
		}).collect(JsonCollectors.ofArray()));
		json.put("springs", mRelations.entrySet().stream().map(e -> {
			JsonObject o = (JsonObject) e.getValue().save();
			
			o.putInt("relation", mEnv.relations().indexOf(e.getKey()));
			
			return o;
		}).collect(JsonCollectors.ofArray()));
		
		return json;
	}
	
	@Override
	public void load(JsonValue json)
	{
		JsonObject obj = (JsonObject) json;
		
		mEnv.load(obj.get("environment"));
		
		mActors.clear();
		mRelations.clear();
		
		obj.getArray("nodes").stream().forEach(v -> {
			JsonObject o = (JsonObject) v;
			
			Actor a = mEnv.actors().get(o.getInt("actor"));
			String id = o.getString("id");
			int c = o.getInt("count");
			Vec3 p = Vec3.load(o.get("position"));
			
			Entry<Node> e = new Entry<>(new Node(id, p, mWorld));
			
			e.counter = c;
			
			mActors.put(a, e);
		});
		obj.getArray("springs").stream().forEach(v -> {
			JsonObject o = (JsonObject) v;
			
			Relation r = mEnv.relations().get(o.getInt("relation"));
			String id = o.getString("id");
			int c = o.getInt("count");

			Entry<Node> left = mActors.get(r.getLeft());
			Entry<Node> right = mActors.get(r.getRight());
			
			Spring s = new Spring(id, left.value, right.value, mWorld);
			
			s.strengthProperty().setValue(r.orientation());
			
			Entry<Spring> e = new Entry<>(s);
			
			e.counter = c;
			
			mRelations.put(r, e);
		});
	}

	@Override
	public void update(double delta)
	{
		if(mCounter-- == 0)
		{
			mEnv.tick();
			
			mCounter = SIM_SPEED.getValue().intValue();
		}
		
		List<Relation> to_add = new ArrayList<>();
		
		for(Relation r : mEnv.relations())
		{
			Entry<Spring> e = mRelations.get(r);
			
			if(e != null)
			{
				++e.counter;
				
				e.value.strengthProperty().setValue(r.orientation());
			}
			else
			{
				to_add.add(r);
			}
		}
		
		mRelations.entrySet().removeIf(e -> {
			boolean f = --e.getValue().counter == 0;
			
			if(f)
			{
				e.getValue().value.kill();

				--mActors.get(e.getKey().getLeft()).counter;
				--mActors.get(e.getKey().getRight()).counter;
			}
			
			return f;
		});
		
		to_add.forEach(r -> {
			Entry<Node> ea = mActors.get(r.getLeft());
			Entry<Node> eb = mActors.get(r.getRight());
			
			ea = addActor(r.getLeft(), ea, r.getRight(), eb);
			eb = addActor(r.getRight(), eb, r.getLeft(), ea);
			
			Spring s = new Spring("spring_" + r, ea.value, eb.value, mWorld);
			
			s.strengthProperty().setValue(r.orientation());
			
			mRelations.put(r, new Entry<>(s));
		});
		
		for(Iterator<Map.Entry<Actor, Entry<Node>>> i = mActors.entrySet().iterator() ; i.hasNext() ;)
		{
			Map.Entry<Actor, Entry<Node>> e = i.next();
			
			if(e.getValue().test())
			{
				mEnv.actors().remove(e.getValue().value);
				i.remove();
			}
			else
			{
				e.getValue().value.valueProperty().setValue(1 + mEnv.valueOf(e.getKey()));
			}
		}
	}
	
	private Entry<Node> addActor(Actor a, Entry<Node> ea, Actor old, Entry<Node> eold)
	{
		if(ea != null)
		{
			++ea.counter;
			
			return ea;
		}
		else
		{
			Vec3 op = Vec3.ORIGIN;
			double f = 1;
			
			if(eold != null)
			{
				op = eold.value.getPosition();
				f += eold.value.valueProperty().getValue().doubleValue();
			}
			
			op = op
				.add(Vec3.X_AXIS.scale(100 * f * (Math.random() - 0.5)))
				.add(Vec3.Y_AXIS.scale(100 * f * (Math.random() - 0.5)))
				.add(Vec3.Z_AXIS.scale(100 * f * (Math.random() - 0.5)));

			Node n = new Node("node_" + a, op, mWorld);
			
			mActors.put(a, ea = new Entry<>(n));
			
			return ea;
		}
	}
	
	private static class Entry<T extends Entity>
	{
		public final T value;
		public int counter;
		
		public Entry(T n)
		{
			value = n;
			counter = 1;
		}
		
		public boolean test( )
		{
			boolean f = counter == 0;
			
			if(f) value.kill();
			
			return f;
		}
		
		public JsonValue save( )
		{
			JsonObject o = new JsonObject();
			
			o.putString("id", value.getID());
			o.putInt("count", counter);
			o.put("position", value.getPosition().save());
			
			return o;
		}
	}
}
