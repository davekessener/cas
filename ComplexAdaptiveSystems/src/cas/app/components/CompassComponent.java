package cas.app.components;

import cas.app.common.Component;
import cas.app.common.UI;
import cas.app.common.Updateable;
import cas.util.geo.Vec3;
import cas.util.geo.fx.FxGeometry;
import cas.util.props.Properties;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.SubScene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;
import javafx.scene.transform.Rotate;

public class CompassComponent implements Component, UI, Updateable
{
	private final Group mCompass;
	private final FxGeometry mTransform;
	private final SubScene mScene;
	private final Property<Vec3> mView;
	
	public CompassComponent( )
	{
		mCompass = new Group();
		mTransform = new FxGeometry();
		mView = new SimpleObjectProperty<>();

		Box x_axis = createBox(Color.RED);
		Box y_axis = createBox(Color.GREEN);
		Box z_axis = createBox(Color.BLUE);
		
		x_axis.setTranslateY(30);
		y_axis.setTranslateX(-30);
		z_axis.setTranslateX(-30);
		z_axis.setTranslateY(30);
		z_axis.setTranslateZ(-30);
		
		y_axis.getTransforms().add(new Rotate(90, Rotate.Z_AXIS));
		z_axis.getTransforms().add(new Rotate(90, Rotate.Y_AXIS));
		
		mCompass.getChildren().addAll(x_axis, y_axis, z_axis);
		mCompass.getTransforms().add(mTransform.getTransform());
		
		mTransform.move(50, 50, 0);
		
		mView.bind(Properties.get(Properties.PLAYER_VIEW));
		
		mScene = new SubScene(mCompass, 100, 100);

		AnchorPane.setLeftAnchor(mScene, 0d);
		AnchorPane.setTopAnchor(mScene, 0d);
	}

	@Override
	public Node getUI()
	{
		return mScene;
	}

	@Override
	public void update(double delta)
	{
		mTransform.setRotation(mView.getValue().negate());
	}
	
	private static Box createBox(Color c)
	{
		Box box = new Box(60, 3, 3);
		PhongMaterial m = new PhongMaterial();
		
		m.setDiffuseColor(c);
		box.setMaterial(m);
		
		return box;
	}
}
