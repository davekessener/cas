package cas.app.components;

import cas.app.common.Component;
import cas.app.common.Updateable;
import cas.app.control.Key;
import cas.app.control.Keyboard;
import cas.util.geo.Vec3;
import cas.util.geo.fx.FxCamera;
import cas.util.props.Properties;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Camera;
import javafx.scene.PerspectiveCamera;

public class CameraComponent implements Component, Updateable
{
	private final Keyboard mKeyboard;
	private final Camera mView;
	private final FxCamera mCamera;
	private final Property<Number> mFlyingSpeed;
	private final Property<Vec3> mCameraPos, mCameraView;
	private double mMouseX, mMouseY;
	
	public CameraComponent(FxCamera c, Keyboard kb)
	{
		mKeyboard = kb;
		mView = new PerspectiveCamera(true);
		mCamera = c;
		mFlyingSpeed = new SimpleDoubleProperty();
		mCameraPos = new SimpleObjectProperty<>(Vec3.ORIGIN);
		mCameraView = new SimpleObjectProperty<>(Vec3.ORIGIN);
		mMouseX = mMouseY = 0;
		
		mView.setNearClip(1);
		mView.setFarClip(1000000.0);
		mView.getTransforms().add(mCamera.getTransform());
		
		mFlyingSpeed.bind(Properties.get(Properties.FLY_SPEED));
		Properties.get(Properties.PLAYER_POS).bind(mCameraPos);
		Properties.get(Properties.PLAYER_VIEW).bind(mCameraView);
	}
	
	public Camera getCamera( ) { return mView; }
	
	public void update(double dx, double dy)
	{
		mMouseX += dx;
		mMouseY += dy;
	}
	
	@Override
	public void update(double delta)
	{
		double rx = mMouseX, ry = mMouseY;
		double vx = 0, vy = 0, vz = 0;
		double dv = delta * mFlyingSpeed.getValue().doubleValue(), dr = delta * VIEW_SPEED;
		
		mMouseX = mMouseY = 0;
		
		if(mKeyboard.isPressed(Key.FORWARD)) vz += 1;
		if(mKeyboard.isPressed(Key.BACKWARD)) vz -= 1;
		if(mKeyboard.isPressed(Key.LEFT)) vx -= 1;
		if(mKeyboard.isPressed(Key.RIGHT)) vx += 1;
		if(mKeyboard.isPressed(Key.UP)) vy -= 1;
		if(mKeyboard.isPressed(Key.DOWN)) vy += 1;
		
		vx *= dv;
		vy *= dv;
		vz *= dv;
		
		rx *= dr;
		ry *= dr;
		
		mCamera.move(vx, vy, vz);
		mCamera.rotate(rx, ry, 0);
		
		mCameraView.setValue(mCamera.getRotation());
		mCameraPos.setValue(mCamera.getPosition());
	}
	
	private static final double TO_R = Math.PI / 180;
	private static final double VIEW_SPEED = 3 * TO_R;
}
