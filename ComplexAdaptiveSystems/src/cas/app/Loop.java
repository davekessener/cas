package cas.app;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.util.Duration;

public class Loop
{
	public static interface Callback { void trigger(double d); }
	
	private final Callback mCallback;
	private final Timeline mLoop;
	
	public Loop(Duration d, Callback f)
	{
		mCallback = f;
		mLoop = new Timeline();
		
		mLoop.setCycleCount(Timeline.INDEFINITE);
		mLoop.getKeyFrames().add(new KeyFrame(d, e -> mCallback.trigger(d.toSeconds())));
	}
	
	public void start( )
	{
		mLoop.play();
	}
	
	public void stop( )
	{
		mLoop.stop();
	}
}
