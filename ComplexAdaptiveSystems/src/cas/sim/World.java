package cas.sim;

import java.util.ArrayList;
import java.util.List;

import cas.sim.common.Plugin;
import cas.sim.common.Updateable;
import dave.util.log.Logger;

public class World implements Updateable
{
	private final List<Plugin> mPlugins = new ArrayList<>();
	
	public World addPlugin(Plugin p)
	{
		mPlugins.add(p);
		
		return this;
	}
	
	public boolean hasPlugin(String id) { return mPlugins.stream().anyMatch(p -> p.getID().equals(id)); }
	
	@SuppressWarnings("unchecked")
	public <T extends Plugin> T getPlugin(String id) { return (T) mPlugins.stream().filter(p -> p.getID().equals(id)).findAny().get(); }
	
	public Logger logger( ) { return LOG; }

	@Override
	public void update(double delta)
	{
		mPlugins.forEach(p -> p.update(delta));
	}
	
	private static final Logger LOG = Logger.get("sim");
}
