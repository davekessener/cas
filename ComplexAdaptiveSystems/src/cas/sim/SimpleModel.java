package cas.sim;

import cas.sim.common.Model;
import cas.util.geo.fx.FxGeometry;
import javafx.scene.Node;

public class SimpleModel<T extends Node> implements Model
{
	private final T mModel;
	private final FxGeometry mTransform;
	
	public SimpleModel(T raw)
	{
		mModel = raw;
		mTransform = new FxGeometry();
		
		mModel.getTransforms().add(mTransform.getTransform());
	}

	@Override
	public T getModel()
	{
		return mModel;
	}

	@Override
	public FxGeometry getTransform()
	{
		return mTransform;
	}
}
