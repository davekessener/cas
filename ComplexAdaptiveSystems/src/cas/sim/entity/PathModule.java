package cas.sim.entity;

import java.util.function.Function;

import cas.sim.Entity;
import cas.sim.common.Modules;
import cas.util.geo.Vec3;

public class PathModule extends BaseModule
{
	private final Function<Double, Vec3> mCallback;
	private double mTime;
	
	public PathModule(Function<Double, Vec3> f, Entity e)
	{
		super(Modules.Physics.PATH, e);
		
		mCallback = f;
		mTime = 0;
	}
	
	@Override
	public void update(double delta)
	{
		entity().positionProperty().setValue(mCallback.apply(mTime += delta));
	}
}
