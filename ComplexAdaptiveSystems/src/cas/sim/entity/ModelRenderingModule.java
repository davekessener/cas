package cas.sim.entity;

import cas.sim.Entity;
import cas.sim.common.Model;
import cas.sim.common.Modules;
import cas.util.geo.Vec3;
import javafx.scene.transform.Translate;

public class ModelRenderingModule extends BaseModule
{
	private final Model mModel;
	private final Translate mBaseTransform;
	
	public ModelRenderingModule(Model m, Entity e)
	{
		super(Modules.Render.MODEL, e);
		
		mModel = m;
		mBaseTransform = new Translate(0, 0, 0);
		
		mModel.getModel().getTransforms().add(0, mBaseTransform);
		
		entity().positionProperty().addListener(o -> updatePosition());
		
		updatePosition();
	}
	
	private void updatePosition( )
	{
		Vec3 p = entity().getPosition();
		
		mBaseTransform.setX(p.X);
		mBaseTransform.setY(-p.Y);
		mBaseTransform.setZ(p.Z);
	}
	
	public Model model( ) { return mModel; }
}
