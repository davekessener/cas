package cas.sim.entity;

import cas.sim.Entity;
import cas.util.geo.Vec3;
import javafx.beans.property.Property;

public class MassModule extends ForceModule
{
	private final double mMass;
	private Vec3 mVelocity, mAccel;
	
	public MassModule(double m, Entity e)
	{
		super(e);
		
		mMass = m;
		mVelocity = Vec3.ORIGIN;
		mAccel = Vec3.ORIGIN;
	}
	
	public double getMass( ) { return mMass; }
	
	public void applyForce(Vec3 f) { accelerate(f.scale(1 / mMass)); }
	public Vec3 getVelocity( ) { return mVelocity; }
	
	public void accelerate(Vec3 a) { mAccel = mAccel.add(a); }
	public void setAcceleration(Vec3 a) { mAccel = a; }
	public Vec3 getAcceleration( ) { return mAccel; }

	@Override
	public void update(double delta)
	{
		mVelocity = mVelocity.add(mAccel.scale(delta));
		
		Property<Vec3> p = entity().positionProperty();
		
		p.setValue(p.getValue().add(mVelocity.scale(delta)));
		
		mAccel = Vec3.ORIGIN;
	}
}
