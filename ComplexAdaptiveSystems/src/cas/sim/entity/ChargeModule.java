package cas.sim.entity;

import cas.sim.Entity;
import cas.sim.common.Modules;

public class ChargeModule extends BaseModule
{
	private double mCharge;
	
	public ChargeModule(double c, Entity e)
	{
		super(Modules.Physics.CHARGE, e);
		
		mCharge = c;
	}
	
	public double getCharge( ) { return mCharge; }
	public void setCharge(double c) { mCharge = c; }
}
