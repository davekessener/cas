package cas.sim.entity;

import cas.sim.Entity;
import cas.sim.common.Modules;
import cas.util.geo.Vec3;

public class ForceModule extends BaseModule
{
	private Vec3 mForce;
	
	public ForceModule(Entity e)
	{
		super(Modules.Physics.FORCE, e);
		
		mForce = Vec3.ORIGIN;
	}

	public void applyForce(Vec3 f) { mForce = mForce.add(f); }
	public Vec3 getForce( ) { return mForce; }
	public void setForce(Vec3 f) { mForce = f; }
	
	@Override
	public void update(double delta)
	{
		Vec3 v = mForce.scale(delta);
		
		if(v.lengthSqr() >= 0.01)
		{
			entity().positionProperty().setValue(entity().getPosition().add(v));
		}
		
		mForce = Vec3.ORIGIN;
	}
}
