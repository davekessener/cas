package cas.sim.entity;

import cas.sim.Entity;
import cas.sim.common.Module;

public abstract class BaseModule implements Module
{
	private final Entity mEntity;
	private final String mID;
	
	protected BaseModule(String id, Entity e)
	{
		mEntity = e;
		mID = id;
		
		mEntity.addModule(this);
	}
	
	protected Entity entity( ) { return mEntity; }
	
	@Override
	public String getID()
	{
		return mID;
	}
	
	@Override
	public void update(double delta)
	{
	}
}
