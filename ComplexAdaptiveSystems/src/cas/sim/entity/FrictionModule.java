package cas.sim.entity;

import cas.sim.Entity;
import cas.sim.common.Modules;
import cas.util.geo.Vec3;

public class FrictionModule extends BaseModule
{
	private final double mFriction;
	
	public FrictionModule(double co, Entity e)
	{
		super(Modules.Physics.FRICTION, e);
		
		mFriction = co * co;
		
		if(!entity().hasModule(Modules.Physics.FORCE))
			throw new IllegalArgumentException("Friction module of entity " + entity().getID() + " requires mass!");
	}
	
	@Override
	public void update(double delta)
	{
		MassModule m = entity().getModule(Modules.Physics.FORCE);
		Vec3 v = m.getVelocity();
		
		m.accelerate(v.negate().scale(1 - 0.9 * Math.exp(-v.lengthSqr() / mFriction)));
	}
}
