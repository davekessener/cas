package cas.sim.common;

public interface Updateable
{
	public abstract void update(double delta);
}
