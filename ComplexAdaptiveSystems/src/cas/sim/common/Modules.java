package cas.sim.common;

public final class Modules
{
	public static final class Physics
	{
		public static final String CHARGE = "physics.charge";
		public static final String FORCE = "physics.force";
		public static final String FRICTION = "physics.friction";
		public static final String PATH = "physics.path";
		
		private Physics( ) { }
	}
	
	public static final class Render
	{
		public static final String MODEL = "render.model";
		
		private Render( ) { }
	}
	
	private Modules( ) { }
}
