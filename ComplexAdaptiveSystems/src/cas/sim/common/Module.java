package cas.sim.common;

import dave.util.Identifiable;

public interface Module extends Identifiable, Updateable
{
}
