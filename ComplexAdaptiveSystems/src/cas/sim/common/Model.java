package cas.sim.common;

import cas.util.geo.fx.FxGeometry;
import javafx.scene.Node;

public interface Model
{
	public abstract Node getModel( );
	public abstract FxGeometry getTransform( );
}
