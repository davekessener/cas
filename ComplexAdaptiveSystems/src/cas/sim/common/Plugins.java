package cas.sim.common;

public final class Plugins
{
	public static final String RENDER = "render";
	public static final String ENTITY = "entity";
	public static final String GRAVITY = "gravity";
	public static final String COULOMB = "coulomb";
	
	private Plugins( ) { }
}
