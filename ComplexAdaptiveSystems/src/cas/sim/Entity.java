package cas.sim;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cas.sim.common.Module;
import cas.sim.common.Plugins;
import cas.sim.common.Updateable;
import cas.sim.world.EntityPlugin;
import cas.util.geo.Vec3;
import dave.util.Identifiable;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;

public class Entity implements Identifiable, Updateable
{
	private final String mID;
	private final World mWorld;
	private final List<Module> mModules;
	private final Property<Vec3> mPosition;
	private final Property<Boolean> mAlive;
	private final Set<Tag> mTags;
	
	public Entity(String id, Vec3 p, World w)
	{
		mID = id;
		mWorld = w;
		mModules = new ArrayList<>();
		mPosition = new SimpleObjectProperty<>(p);
		mAlive = new SimpleBooleanProperty(true);
		mTags = new HashSet<>();
		
		EntityPlugin ep = mWorld.getPlugin(Plugins.ENTITY);
		
		ep.entities().add(this);
	}
	
	public void kill( ) { mAlive.setValue(false); }
	
	public Property<Vec3> positionProperty( ) { return mPosition; }
	public Property<Boolean> aliveProperty( ) { return mAlive; }
	
	public Vec3 getPosition( ) { return mPosition.getValue(); }
	public boolean isAlive( ) { return mAlive.getValue(); }
	
	public boolean hasTag(Tag t) { return mTags.contains(t); }
	public void addTags(Tag ... t) { mTags.addAll(Arrays.asList(t)); }
	
	public void addModule(Module m)
	{
		if(hasModule(m.getID()))
			throw new IllegalArgumentException("Entity " + getID() + " already has a " + m.getID() + " module!");
		
		mModules.add(m);
	}
	
	public boolean hasModule(String id) { return mModules.stream().anyMatch(m -> m.getID().equals(id)); }
	@SuppressWarnings("unchecked")
	public <T extends Module> T getModule(String id) { return (T) mModules.stream().filter(m -> m.getID().equals(id)).findFirst().get(); }
	
	public World getWorld( ) { return mWorld; }

	@Override
	public String getID()
	{
		return mID;
	}

	@Override
	public void update(double delta)
	{
		mModules.forEach(m -> m.update(delta));
	}
	
	public static enum Tag
	{
	}
}
