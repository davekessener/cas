package cas.sim.world;

import cas.sim.Entity;
import cas.sim.World;
import cas.sim.common.Modules;
import cas.sim.common.Plugins;
import cas.sim.entity.ForceModule;
import cas.util.geo.Vec3;

public class GravityPlugin extends BasePlugin
{
	private final Vec3 mCenter;
	private final double mForce;
	
	public GravityPlugin(Vec3 center, double f, World w)
	{
		super(Plugins.GRAVITY, w);
		
		mCenter = center;
		mForce = f;
	}
	
	@Override
	public void update(double delta)
	{
		EntityPlugin ep = world().getPlugin(Plugins.ENTITY);
		
		for(Entity e : ep.entities())
		{
			if(e.hasModule(Modules.Physics.FORCE))
			{
				ForceModule m = e.getModule(Modules.Physics.FORCE);
				Vec3 d = mCenter.add(e.getPosition().negate());
				double l = d.length();
				
				if(l == 0)
					continue;
				
				d = d.scale(1.0 / l).scale(mForce).scale(1 - Math.exp(-l));
				
				m.applyForce(d);
			}
		}
	}
}
