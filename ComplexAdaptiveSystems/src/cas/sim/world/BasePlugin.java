package cas.sim.world;

import cas.sim.World;
import cas.sim.common.Plugin;

public abstract class BasePlugin implements Plugin
{
	private final String mID;
	private final World mWorld;
	
	protected BasePlugin(String id, World w)
	{
		mID = id;
		mWorld = w;
		
		mWorld.addPlugin(this);
	}
	
	protected World world( ) { return mWorld; }
	
	@Override
	public String getID( )
	{
		return mID;
	}

	@Override
	public void update(double delta)
	{
	}
}
