package cas.sim.world;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;

import cas.sim.Entity;
import cas.sim.World;
import cas.sim.common.Plugins;

public class EntityPlugin extends BasePlugin
{
	private final List<Entity> mEntities;
	private final Predicate<Entity> mDeath;
	
	public EntityPlugin(World w) { this(w, e -> !e.isAlive()); }
	public EntityPlugin(World w, Predicate<Entity> f)
	{
		super(Plugins.ENTITY, w);
		
		mEntities = new LinkedList<>();
		mDeath = f;
	}
	
	public List<Entity> entities( ) { return mEntities; }

	@Override
	public void update(double delta)
	{
		mEntities.forEach(e -> e.update(delta));
		mEntities.removeIf(mDeath);
	}
}
