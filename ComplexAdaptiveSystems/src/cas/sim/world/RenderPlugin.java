package cas.sim.world;

import cas.sim.Entity;
import cas.sim.World;
import cas.sim.common.Modules;
import cas.sim.common.Plugins;
import cas.sim.entity.ModelRenderingModule;
import javafx.scene.Group;
import javafx.scene.Parent;

public class RenderPlugin extends BasePlugin
{
	private final Group mScene;
	private Group mFrame;
	
	public RenderPlugin(World w)
	{
		super(Plugins.RENDER, w);
		
		mScene = new Group();
		mFrame = new Group();
		
		mScene.getChildren().add(mFrame);
	}
	
	public Parent getScene( ) { return mScene; }
	
	@Override
	public void update(double delta)
	{
		Group f = new Group();
		EntityPlugin ep = world().getPlugin(Plugins.ENTITY);
		
		for(Entity e : ep.entities())
		{
			if(e.hasModule(Modules.Render.MODEL))
			{
				ModelRenderingModule m = e.getModule(Modules.Render.MODEL);
				
				f.getChildren().add(m.model().getModel());
			}
		}
		
		mScene.getChildren().remove(mFrame);
		mScene.getChildren().add(mFrame = f);
	}
}
