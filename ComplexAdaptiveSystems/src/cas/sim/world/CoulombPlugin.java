package cas.sim.world;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import cas.sim.Entity;
import cas.sim.World;
import cas.sim.common.Modules;
import cas.sim.common.Plugins;
import cas.sim.entity.ChargeModule;
import cas.sim.entity.ForceModule;
import cas.util.geo.Vec3;
import dave.util.log.Severity;

public class CoulombPlugin extends BasePlugin
{
	private final double mForce;
	
	public CoulombPlugin(double f, World w)
	{
		super(Plugins.COULOMB, w);
		
		mForce = f;
	}
	
	@Override
	public void update(double delta)
	{
		EntityPlugin ep = world().getPlugin(Plugins.ENTITY);
		List<Entity> all = new ArrayList<>();

		for(Entity e : ep.entities())
		{
			if(e.hasModule(Modules.Physics.CHARGE) && e.hasModule(Modules.Physics.FORCE))
			{
				all.add(e);
			}
		}
		
		for(ListIterator<Entity> i1 = all.listIterator() ; i1.hasNext() ;)
		{
			Entity e1 = i1.next();
			
			for(ListIterator<Entity> i2 = all.listIterator(i1.nextIndex()) ; i2.hasNext() ;)
			{
				Entity e2 = i2.next();

				ForceModule m1 = e1.getModule(Modules.Physics.FORCE);
				ForceModule m2 = e2.getModule(Modules.Physics.FORCE);

				ChargeModule ch1 = e1.getModule(Modules.Physics.CHARGE);
				ChargeModule ch2 = e2.getModule(Modules.Physics.CHARGE);
				double c1 = ch1.getCharge();
				double c2 = ch2.getCharge();
				
				Vec3 v = e2.getPosition().add(e1.getPosition().negate());
				double d = v.lengthSqr();
				
				double min = 10 * Math.max(c1, c2);
				
				if(d > min * min) continue;
				else if(d == 0)
				{
					world().logger().log(Severity.WARNING, "Two entities occupy the same space! @%s", e1.getPosition().toString());
				}
				else
				{
					double f = mForce * ch1.getCharge() * ch2.getCharge() / d;
					
					v = v.scale(f);

					m1.applyForce(v.negate());
					m2.applyForce(v);
				}
			}
		}
	}
}
