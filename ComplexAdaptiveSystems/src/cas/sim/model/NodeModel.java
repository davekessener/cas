package cas.sim.model;

import cas.sim.SimpleModel;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Sphere;

public class NodeModel extends SimpleModel<Sphere>
{
	private final PhongMaterial mMaterial;
	
	public NodeModel( )
	{
		super(new Sphere((MIN + MAX) / 2));
		
		mMaterial = new PhongMaterial();
		
		getModel().setMaterial(mMaterial);
	}
	
	public void setSize(double v) { getModel().setRadius(MIN + (MAX - MIN) * v); }
	public void setColor(Color c) { mMaterial.setDiffuseColor(c); }
	
	private static final double MIN = 5, MAX = 10;
}
