package cas.sim.model;

import cas.sim.Entity;
import cas.sim.World;
import cas.sim.common.Modules;
import cas.sim.entity.ForceModule;
import cas.sim.entity.ModelRenderingModule;
import cas.util.geo.Vec3;
import dave.util.log.Logger;
import dave.util.log.Severity;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.paint.Color;

public class Spring extends Entity
{
	private final Entity mLeft, mRight;
	private final Property<Number> mStrength;
	private final SpringModel mModel;
	private Vec3 mConnection;
	private boolean mDirty;
	
	public Spring(String id, Entity a, Entity b, World w)
	{
		super(id, Vec3.ORIGIN, w);
		
		mLeft = a;
		mRight = b;
		mStrength = new SimpleDoubleProperty();
		mModel = new SpringModel();
		mConnection = null;
		mDirty = true;
		
		if(mLeft == null || mRight == null)
			throw new NullPointerException();

		mLeft.positionProperty().addListener(o -> mDirty = true);
		mRight.positionProperty().addListener(o -> mDirty = true);
		
		new ModelRenderingModule(mModel, this);
	}
	
	private double getStrength( ) { return Math.min(1, Math.max(-1, mStrength.getValue().doubleValue())); }
	private double getThickness( ) { return 0.5 + 2.5 * Math.abs(getStrength()); }
	private double getLength( ) { return 20 + 75 * (1 - getStrength()); }
	
	private Color getColor( )
	{
		double p = (getStrength() + 1) / 2;
		
		double r = 1 - p;
		double g = p;
		double b = Math.min(r, g);
		
		return Color.rgb((int) (255 * r), (int) (255 * g), (int) (255 * b));
	}
	
	private void recalculate( )
	{
		Vec3 l = mLeft.getPosition();
		Vec3 r = mRight.getPosition();
		Vec3 n = l.add(r).scale(0.5);

		mConnection = r.add(l.negate());

		positionProperty().setValue(n);
		
		mModel.setThickness(getThickness());
		mModel.setColor(getColor());
		
		mModel.update(mConnection);
		mDirty = false;
	}
	
	@Override
	public void update(double delta)
	{
		if(mDirty) recalculate();
		
		double f = mConnection.length() / getLength();
		
		if(Double.isFinite(f))
		{
			Vec3 v = mConnection.scale(f * getThickness());
	
			ForceModule m1 = mLeft.getModule(Modules.Physics.FORCE);
			ForceModule m2 = mRight.getModule(Modules.Physics.FORCE);
			
			m1.applyForce(v);
			m2.applyForce(v.negate());
		}
		else
		{
			Logger.DEFAULT.log(Severity.ERROR, "Spring " + this + " between " + mLeft.getPosition() + " and " + mRight.getPosition() + " can't calculate forces!");
		}
		
		super.update(delta);
	}
	
	public Property<Number> strengthProperty( ) { return mStrength; }
}
