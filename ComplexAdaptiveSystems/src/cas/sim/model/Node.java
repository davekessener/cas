package cas.sim.model;

import cas.sim.Entity;
import cas.sim.World;
import cas.sim.common.Modules;
import cas.sim.entity.ChargeModule;
import cas.sim.entity.FrictionModule;
import cas.sim.entity.MassModule;
import cas.sim.entity.ModelRenderingModule;
import cas.util.geo.Vec3;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleDoubleProperty;

public class Node extends Entity
{
	private final Property<Number> mValue;
	private final NodeModel mModel;
	
	public Node(String id, Vec3 p, World w)
	{
		super(id, p, w);
		
		mValue = new SimpleDoubleProperty();
		mModel = new NodeModel();
		
		new ModelRenderingModule(mModel, this);
		new MassModule(10, this);
		new FrictionModule(100, this);
		new ChargeModule(CHARGE, this);
	}
	
	public Property<Number> valueProperty( ) { return mValue; }
	
	@Override
	public void update(double delta)
	{
		ChargeModule ch = this.getModule(Modules.Physics.CHARGE);
		double v = mValue.getValue().doubleValue();
		
		mModel.setSize(v * 1.3);
		ch.setCharge(CHARGE * (1.5 + 8 * v));
		
		super.update(delta);
	}
	
	private static final double CHARGE = 8;
}
