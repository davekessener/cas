package cas.sim.model;

import cas.sim.SimpleModel;
import cas.util.geo.Vec3;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Cylinder;

public class SpringModel extends SimpleModel<Cylinder>
{
	private double mThickness;
	private PhongMaterial mMaterial;
	
	public SpringModel( )
	{
		super(new Cylinder(1, 1, 5));
		
		mThickness = 1;
		mMaterial = new PhongMaterial();
		
		mMaterial.setDiffuseColor(Color.WHITE);
		
		getModel().setMaterial(mMaterial);
	}
	
	public void setColor(Color c) { mMaterial.setDiffuseColor(c); }
	public void setThickness(double v) { mThickness = v; }
	
	public void update(Vec3 lookat)
	{
		Cylinder c = getModel();
		
		c.setRadius(mThickness / 2);
		c.setHeight(lookat.length());
		
		Vec3 r = lookat.lookAt();
		
		getTransform().setRotation(new Vec3(r.X, r.Y + Math.PI / 2, r.Z));
	}
}
