package cas.model;

import dave.util.Identifiable;

public interface Rule extends Identifiable
{
	public abstract void apply(Environment e);
}
