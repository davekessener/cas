package cas.model.rules;

import cas.model.Environment;

public class Rule_211 extends BaseRule
{
	public Rule_211( )
	{
		super("rule_211");
	}
	
	@Override
	public void apply(Environment e)
	{
		e.relations().stream().forEach(r -> {
			if(Math.random() < 0.025)
			{
				for(int i = 0 ; i < e.size() ; ++i)
				{
					double vl = r.getLeft().get(i), vr = r.getRight().get(i);
					double f = 1;
					
					if(vl * vr < 0)
					{
						f -= 0.25;
					}
					else
					{
						f += 0.25;
					}
	
					r.getLeft().set(i, vl * f);
					r.getRight().set(i, vr * f);
				}
			}
		});
	}
}
