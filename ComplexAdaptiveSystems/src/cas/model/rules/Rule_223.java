package cas.model.rules;

import java.util.HashSet;
import java.util.Set;

import cas.model.Actor;
import cas.model.Environment;
import cas.model.Relation;

public class Rule_223 extends BaseRule
{
	public Rule_223( )
	{
		super("rule_223");
	}
	
	@Override
	public void apply(Environment e)
	{
		if(Math.random() < 0.1 * (1 - 1 / (e.relations().size() / (e.actors().size() / 2 + 10) + 1)))
		{
			e.relations().stream()
				.sorted((r1, r2) -> Double.compare(Math.abs(r1.orientation()), Math.abs(r2.orientation())))
				.filter(r -> Math.random() < 0.5).findFirst()
				.ifPresent(r -> {
					if(Math.abs(r.orientation()) < 0.15)
					{
						Set<Relation> open = new HashSet<>(e.relations());
						Set<Actor> current = new HashSet<>();
						boolean deleteable = false;
						
						open.remove(r);
						current.add(r.getLeft());
						
						while(!open.isEmpty())
						{
							boolean valid = false;
							
							for(Relation rr : open)
							{
								boolean left = current.contains(rr.getLeft());
								boolean right = current.contains(rr.getRight());
								
								if(left || right)
								{
									open.remove(rr);
									
									if(left && !right)
									{
										current.remove(rr.getLeft());
										current.add(rr.getRight());
									}
									else if(right && !left)
									{
										current.remove(rr.getRight());
										current.add(rr.getLeft());
									}
									
									deleteable = current.contains(r.getRight());
									valid = true;
									
									break;
								}
							}
							
							if(!valid || deleteable) break;
						}
						
						if(deleteable)
						{
							e.relations().remove(r);
						}
					}
				});
		}
	}
}
