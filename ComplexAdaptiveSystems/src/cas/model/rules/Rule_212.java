package cas.model.rules;

import cas.model.Actor;
import cas.model.Environment;

public class Rule_212 extends BaseRule
{
	public Rule_212( )
	{
		super("rule_212");
	}
	
	@Override
	public void apply(Environment e)
	{
		e.relations().stream()
			.sorted((r1, r2) -> Double.compare(r2.orientation(), r1.orientation()))
			.filter(r -> Math.random() < 0.5).findFirst()
			.ifPresent(r -> {
				Actor a = r.getLeft(), b = r.getRight();
				double va = e.valueOf(a), vb = e.valueOf(b);
				
				if(va > vb)
				{
					double vt = va; va = vb; vb = vt;
					Actor t = a; a = b; b = t;
				}
				
				if(Math.random() < vb / (va + vb))
				{
					int j = -1; double v = 0;
					for(int i = 0 ; i < e.size() ; ++i)
					{
						double t = Math.abs(a.get(i) - b.get(i));
						
						if(j == -1 || t > v)
						{
							j = i;
							v = t;
						}
					}
					
					a.set(j, a.get(j) + (b.get(j) - a.get(j)) * 0.5);
				}
			});
	}
}
