package cas.model.rules;

import java.util.ArrayDeque;
import java.util.HashSet;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Collectors;

import cas.model.Actor;
import cas.model.Environment;

public class Rule_222 extends BaseRule
{
	public Rule_222( )
	{
		super("rule_222");
	}
	
	@Override
	public void apply(Environment e)
	{
		if(Math.random() < 0.075)
		{
			e.relations().stream()
				.sorted((r1, r2) -> Double.compare(r2.orientation(), r1.orientation()))
				.filter(r -> Math.random() < 0.5).findFirst()
				.ifPresent(r -> {
					Actor a = r.getLeft(), b = r.getRight();
					double va = e.valueOf(a), vb = e.valueOf(b);
					
					if(va < vb)
					{
						double vt = va; va = vb; vb = vt;
						Actor t = a; a = b; b = t;
					}
					
					if(va < 3 && va < 1.5 * vb) return;
					
					final Actor sup = a;
					Set<Actor> ex = e.neighbors(a).collect(Collectors.toSet());
					Queue<Actor> open = new ArrayDeque<>();
					Set<Actor> pot = new HashSet<>();
					Set<Actor> done = new HashSet<>();
					
					done.add(a);
					open.add(b);
					while(!open.isEmpty())
					{
						Actor t = open.poll();
						
						done.add(t);
						
						if(ex.contains(t))
						{
							e.neighbors(t).filter(aa -> !done.contains(aa)).forEach(aa -> open.add(aa));
						}
						else
						{
							pot.add(t);
						}
					}
					
					pot.forEach(aa -> {
						if(Math.random() < 0.5)
						{
							e.connect(sup, aa);
						}
					});
				});
		}
	}
}
