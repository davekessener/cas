package cas.model.rules;

import cas.model.Rule;

public abstract class BaseRule implements Rule
{
	private final String mID;
	
	protected BaseRule(String id)
	{
		mID = id;
	}
	
	@Override
	public String getID( )
	{
		return mID;
	}
}
