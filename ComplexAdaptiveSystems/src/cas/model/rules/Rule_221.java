package cas.model.rules;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Collectors;

import cas.model.Actor;
import cas.model.Environment;
import cas.model.Relation;
import cas.util.Utils;

public class Rule_221 extends BaseRule
{
	public Rule_221( )
	{
		super("rule_221");
	}
	
	@Override
	public void apply(Environment e)
	{
		if(Math.random() < 0.0025)
		{
			List<Set<Actor>> pot = new ArrayList<>();
			Queue<Relation> open = new ArrayDeque<>(e.relations().stream().filter(r -> r.orientation() > 0.5).collect(Collectors.toSet()));
			
			while(!open.isEmpty())
			{
				Queue<Relation> tmp = new ArrayDeque<>();
				Set<Actor> actors = new HashSet<>();
				Relation current = open.poll();
				int c = 1;
				
				actors.add(current.getLeft());
				actors.add(current.getRight());
				
				for(Relation r : open)
				{
					if(actors.contains(r.getLeft()))
					{
						actors.add(r.getRight());
						++c;
					}
					else if(actors.contains(r.getRight()))
					{
						actors.add(r.getLeft());
						++c;
					}
					else
					{
						tmp.add(r);
					}
				}
				
				if(c >= 2 * actors.size())
				{
					pot.add(actors);
				}
				
				open = tmp;
			}
			
			if(!pot.isEmpty() && Math.random() < 0.1)
			{
				Set<Actor> actors = Utils.any(pot);

				Actor n = e.create();
				double[] v = new double[n.size()];
				
				for(Actor a : actors)
				{
					e.connect(a, n);
					
					for(int i = 0 ; i < v.length ; ++i)
					{
						v[i] += a.get(i);
					}
				}
				
				for(int i = 0 ; i < n.size() ; ++i)
				{
					n.set(i, v[i] / actors.size());
				}
			}
		}
	}
}
