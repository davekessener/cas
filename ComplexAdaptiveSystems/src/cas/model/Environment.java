package cas.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import dave.json.JsonBuilder;
import dave.json.JsonArray;
import dave.json.JsonCollectors;
import dave.json.JsonObject;
import dave.json.JsonString;
import dave.json.JsonValue;
import dave.json.Loadable;
import dave.json.Saveable;
import dave.util.log.Logger;
import dave.util.log.Severity;

public class Environment implements Saveable, Loadable
{
	private final List<Actor> mActors;
	private final List<Relation> mRelations;
	private final List<Rule> mRules;
	private final int mSize;
	
	public Environment(int n)
	{
		mActors = new ArrayList<>();
		mRelations = new ArrayList<>();
		mRules = new ArrayList<>();
		mSize = n;
	}
	
	public int size( ) { return mSize; }
	public List<Actor> actors( ) { return mActors; }
	public List<Relation> relations( ) { return mRelations; }
	
	@Override
	public JsonValue save( )
	{
		JsonObject json = new JsonObject();
		JsonArray actors = mActors.stream().map(Actor::save).collect(JsonCollectors.ofArray());
		JsonArray relations = mRelations.stream().map(r ->
			(new JsonBuilder())
				.putInt("left", mActors.indexOf(r.getLeft()))
				.putInt("right", mActors.indexOf(r.getRight()))
				.toJSON())
			.collect(JsonCollectors.ofArray());
		JsonArray rules = mRules.stream().map(r -> new JsonString(r.getID())).collect(JsonCollectors.ofArray());
		
		json.put("actors", actors);
		json.put("relations", relations);
		json.put("rules", rules);
		
		return json;
	}
	
	@Override
	public void load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		List<String> cmp = mRules.stream().map(Rule::getID).collect(Collectors.toList());
		
		o.getArray("rules").stream().map(v -> ((JsonString) v).get()).forEach(rid -> {
			int idx = cmp.indexOf(rid);
			
			if(idx == -1)
			{
				LOG.log(Severity.ERROR, "Loading environment that expects %s which is missing!", rid);
			}
			else
			{
				cmp.remove(idx);
			}
		});
		
		if(!cmp.isEmpty())
		{
			LOG.log(Severity.WARNING, "Loading environment that doesn't know rules %s!", cmp.stream().collect(Collectors.joining(", ")));
		}
		
		mActors.clear();
		mRelations.clear();
		
		o.getArray("actors").forEach(v -> mActors.add(Actor.load(v)));
		o.getArray("relations").forEach(v -> {
			JsonObject r = (JsonObject) v;
			Actor left = mActors.get(r.getInt("left"));
			Actor right = mActors.get(r.getInt("right"));
			
			mRelations.add(new Relation(left, right));
		});
	}
	
	public Actor create( )
	{
		Actor a = new Actor(mSize);
		
		mActors.add(a);
		
		return a;
	}
	
	public Relation connect(Actor a, Actor b)
	{
		if(a == b)
			throw new IllegalArgumentException();
		
		Relation r = mRelations.stream().filter(rr -> rr.involves(a) && rr.involves(b)).findFirst().orElse(null);
		
		if(r == null)
		{
			mRelations.add(r = new Relation(a, b));
		}
		
		return r;
	}
	
	public Environment add(Rule r)
	{
		mRules.add(r);
		
		return this;
	}
	
	public double valueOf(Actor a)
	{
		return mRelations.stream().filter(r -> r.involves(a)).mapToDouble(r -> r.orientation()).map(v -> v * v).sum();
	}
	
	public Stream<Actor> neighbors(Actor a)
	{
		return relations().stream().filter(r -> r.involves(a)).map(r -> r.other(a));
	}
	
	public void tick( )
	{
		for(Rule r : mRules)
		{
			r.apply(this);
		}
	}
	
	private static final Logger LOG = Logger.get("env");
}
