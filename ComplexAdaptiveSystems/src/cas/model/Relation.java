package cas.model;

import java.util.stream.IntStream;

public class Relation
{
	private final Actor mLeft, mRight;
	private final int mSize;
	
	public Relation(Actor a1, Actor a2)
	{
		mLeft = a1;
		mRight = a2;
		mSize = mLeft.size();
		
		if(mLeft == null || mRight == null)
			throw new NullPointerException();
		
		if(mLeft.size() != mRight.size())
			throw new IllegalArgumentException("" + mLeft.size() + " != " + mRight.size());
	}
	
	public Actor getLeft( ) { return mLeft; }
	public Actor getRight( ) { return mRight; }
	
	public boolean involves(Actor a) { return mLeft == a || mRight == a; }
	public Actor other(Actor a) { return mLeft == a ? mRight : mLeft; }
	
	public double orientation( ) { return IntStream.range(0, mSize).mapToDouble(i -> mLeft.get(i) * mRight.get(i)).sum() / mSize; }
}
