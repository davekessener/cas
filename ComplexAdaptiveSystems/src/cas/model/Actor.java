package cas.model;

import java.util.Arrays;

import dave.json.Container;
import dave.json.JsonArray;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;

@Container
public class Actor implements Saveable
{
	private final double[] mInterests;
	
	public Actor(int n)
	{
		mInterests = new double[n];
	}
	
	@Override
	@Saver
	public JsonValue save( )
	{
		JsonObject json = new JsonObject();
		
		json.put("interests", new JsonArray(mInterests));
		
		return json;
	}
	
	@Loader
	public static Actor load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;
		double[] interests = o.getArray("interests").asDoubles();
		
		Actor a = new Actor(interests.length);
		
		for(int i = 0 ; i < a.size() ; ++i)
		{
			a.set(i, interests[i]);
		}
		
		return a;
	}
	
	public int size( ) { return mInterests.length; }
	
	public double get(int i) { return mInterests[i]; }
	public void set(int i, double v) { mInterests[i] = Math.max(-1, Math.min(1, v)); }
	
	public double spectrum( ) { return Arrays.stream(mInterests).map(v -> v * v).sum(); }
}
