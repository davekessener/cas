package cas;

import cas.app.common.App;
import cas.app.internal.EmptyApp;
import javafx.application.Application;
import javafx.stage.Stage;

public class Start extends Application
{
	private static App application;
	
	public static void main(String[] args)
	{
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception
	{
		application = new EmptyApp(stage);
	}
	
	@Override
	public void stop( )
	{
		System.out.println("Shutting down ...");
		application.stop();
	}
}
