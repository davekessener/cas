package cas.util;

public class SevereException extends RuntimeException
{
	private static final long serialVersionUID = -4132301601660062221L;

	public SevereException(Exception e)
	{
		super(e);
	}
}
