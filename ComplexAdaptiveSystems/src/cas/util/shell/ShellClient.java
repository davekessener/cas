package cas.util.shell;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class ShellClient implements Consumer<String>
{
	private final List<Command> mCommands = new ArrayList<>();
	
	public ShellClient add(Command cmd)
	{
		mCommands.add(cmd);
		
		return this;
	}
	
	public Stream<Command> commands( ) { return mCommands.stream(); }
	
	@Override
	public void accept(String line)
	{
		try
		{
			Invocation call = Parser.parse(line);
			
			for(Command cmd : mCommands)
			{
				if(cmd.matches(call.getCommand()))
				{
					try
					{
						cmd.call(call.getArguments());
					}
					catch(ExecutionException e)
					{
						e.printStackTrace();
						
						System.out.println("Error while executing '" + call.getCommand() + "': " + e.getMessage());
					}
					
					return;
				}
			}
			
			System.out.println("Unknown command: " + line);
		}
		catch(ParseException e)
		{
			System.out.println("Incomprehensible command '" + line + "'!");
			System.err.println(e.getMessage());
		}
	}
}
