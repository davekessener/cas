package cas.util.shell;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class Parser
{
	private final List<String> mBuffer;
	private String mLine;
	private boolean mCheck;
	
	private Parser(String l)
	{
		mBuffer = new LinkedList<>();
		mLine = l;
		mCheck = false;
	}
	
	private boolean done( )
	{
		return mLine.isEmpty();
	}
	
	private void check( ) throws ParseException
	{
		if(!mCheck)
			throw new ParseException("Malformed command string @" + mBuffer.size() + ": " + mLine);
		
		mCheck = false;
	}
	
	private void process(Pattern p, Function<String, String> f)
	{
		Matcher m = p.matcher(mLine);
		
		if(m.find())
		{
			String s = f.apply(m.group());
			
			if(s != null)
			{
				mBuffer.add(s);
			}
			
			mLine = m.replaceFirst("");
			mCheck = true;
		}
	}
	
	private Invocation get( ) throws ParseException
	{
		if(mBuffer.isEmpty())
			throw new ParseException("Empty command string!");
		
		String cmd = mBuffer.remove(0);
		
		return new Invocation(cmd, mBuffer.toArray(new String[mBuffer.size()]));
	}

	public static Invocation parse(String line) throws ParseException
	{
		Parser p = new Parser(line);
		
		while(!p.done())
		{
			p.process(PTRN_WS, s -> null);
			p.process(PTRN_ID, id -> id.toLowerCase());
			p.process(PTRN_NUMBER, no -> no);
			p.process(PTRN_STRING, s -> s.substring(1, s.length() - 1));
			
			p.check();
		}
		
		return p.get();
	}

	private static final Pattern PTRN_ID = Pattern.compile("^[a-zA-Z][a-zA-Z0-9_-]*");
	private static final Pattern PTRN_NUMBER = Pattern.compile("^([+-]?[0-9]+(\\.[0-9]+)?([eE][+-]?[0-9]+)?)|(($|0x)[0-9a-fA-F]+)");
	private static final Pattern PTRN_STRING = Pattern.compile("^\"([^\"]*(\\\\\")*)*\"");
	private static final Pattern PTRN_WS = Pattern.compile("^[ \t]+");
}
