package cas.util.shell;

public class ParseException extends Exception
{
	private static final long serialVersionUID = -2718277909453532717L;

	public ParseException(String msg)
	{
		super(msg);
	}
}
