package cas.util.shell;

import java.util.Arrays;

public class Invocation
{
	private final String mCmd;
	private final String[] mArgs;
	
	public Invocation(String cmd, String ... args)
	{
		mCmd = cmd;
		mArgs = Arrays.copyOf(args, args.length);
	}
	
	public String getCommand( ) { return mCmd; }
	public String[] getArguments( ) { return mArgs; }
}
