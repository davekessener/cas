package cas.util.shell;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class AliasCommand implements Command
{
	private final Command mCallback;
	private final List<String> mAliases;
	
	public AliasCommand(Command f, String ... a)
	{
		mCallback = f;
		mAliases = Arrays.asList(a);
	}
	
	@Override
	public String help( )
	{
		return mCallback.help();
	}

	@Override
	public boolean matches(String cmd)
	{
		if(mCallback.matches(cmd))
			return true;
		
		for(String alias : mAliases)
		{
			if(alias.equalsIgnoreCase(cmd))
				return true;
		}
		
		return false;
	}

	@Override
	public void call(String[] args) throws ExecutionException
	{
		mCallback.call(args);
	}
}
