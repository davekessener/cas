package cas.util.shell;

import java.util.concurrent.ExecutionException;

public interface Command
{
	public abstract String help( );
	public abstract boolean matches(String cmd);
	public abstract void call(String[] args) throws ExecutionException;
}
