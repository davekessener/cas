package cas.util.shell;

import java.util.concurrent.ExecutionException;

public class SimpleCommand implements Command
{
	public static interface Executable { public void run(String[] args) throws ExecutionException; }
	
	private final String mCommand, mHelp;
	private final Executable mCallback;
	
	public SimpleCommand(String cmd, String h, Runnable f) { this(cmd, h, args -> f.run()); }
	public SimpleCommand(String cmd, String h, Executable f)
	{
		mCommand = cmd;
		mHelp = h;
		mCallback = f;
	}
	
	@Override
	public String help( )
	{
		return mHelp;
	}

	@Override
	public boolean matches(String cmd)
	{
		return mCommand.equalsIgnoreCase(cmd);
	}

	@Override
	public void call(String[] args) throws ExecutionException
	{
		mCallback.run(args);
	}
}
