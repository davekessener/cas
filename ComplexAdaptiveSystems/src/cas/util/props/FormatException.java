package cas.util.props;

public class FormatException extends Exception
{
	private static final long serialVersionUID = -6582557393050884226L;
	
	public FormatException(Class<?> e, String v)
	{
		super(String.format("Can't cast '%s' to %s!", v, e.getName()));
	}
}
