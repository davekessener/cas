package cas.util.props;

import java.util.HashMap;
import java.util.Map;

import cas.util.geo.Vec3;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;

public class Properties
{
	private static final Map<String, Property<?>> mProperties = new HashMap<>();
	
	@SuppressWarnings("unchecked")
	public static <T> Property<T> get(String id)
	{
		Property<T> p = (Property<T>) mProperties.get(id);
		
		if(p == null)
			throw new IllegalArgumentException(id);
		
		return p;
	}
	
	public static final String FLY_SPEED = "fly-speed";
	public static final String PLAYER_POS = "player-pos";
	public static final String PLAYER_VIEW = "player-view";
	public static final String SIM_SPEED = "simulation-speed";
	
	private static final class Defaults
	{
		public static final double FLY_SPEED = 250;
		public static final Vec3 PLAYER_POS = Vec3.ORIGIN;
		public static final Vec3 PLAYER_VIEW = Vec3.ORIGIN;
		public static final int SIM_SPEED = 5;
	}
	
	static
	{
		mProperties.put(FLY_SPEED, new SimpleDoubleProperty(Defaults.FLY_SPEED));
		mProperties.put(PLAYER_POS, new SimpleObjectProperty<>(Defaults.PLAYER_POS));
		mProperties.put(PLAYER_VIEW, new SimpleObjectProperty<>(Defaults.PLAYER_VIEW));
		mProperties.put(SIM_SPEED, new SimpleIntegerProperty(Defaults.SIM_SPEED));
	}
	
	private Properties( ) { }
}
