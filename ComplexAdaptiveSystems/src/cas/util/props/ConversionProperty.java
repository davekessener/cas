package cas.util.props;

import java.util.function.Function;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;

public class ConversionProperty<S, T> extends ReadOnlyObjectWrapper<T>
{
	public ConversionProperty(ObservableValue<S> obj, Function<S, T> f)
	{
		obj.addListener(o -> {
			this.set(f.apply(obj.getValue()));
		});
	}
}
