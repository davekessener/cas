package cas.util.props;

public class ValueHolder<T> implements Value
{
	public static interface Producer<T> { T produce(String v) throws FormatException; }
	
	private final Producer<T> mProducer;
	private T mValue;
	
	public ValueHolder(T def, Producer<T> f)
	{
		mValue = def;
		mProducer = f;
	}
	
	public void set(T v) { mValue = v; }
	public T get( ) { return mValue; }
	
	@Override
	public void parse(String v) throws FormatException
	{
		mValue = mProducer.produce(v);
	}
	
	public static ValueHolder<Double> ofDouble( )
	{
		return new ValueHolder<>(0, v -> {
			try
			{
				return Double.parseDouble(v);
			}
			catch(NumberFormatException e)
			{
				throw new FormatException(double.class, v);
			}
		});
	}
	
	public static ValueHolder<Integer> ofInt( )
	{
		return new ValueHolder<>(0, v -> {
			try
			{
				return Integer.parseInt(v);
			}
			catch(NumberFormatException e)
			{
				throw new FormatException(int.class, v);
			}
		});
	}
	
	public static ValueHolder<String> ofString( )
	{
		return new ValueHolder<>("", v -> {
			if(v.length() >= 2 && v.charAt(0) == '"' && v.charAt(v.length() - 1) == '"')
			{
				v = v.substring(1, v.length() - 1);
			}
			
			return v;
		});
	}
}
