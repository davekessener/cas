package cas.util.props;

public interface Value
{
	public abstract void parse(String v) throws FormatException;
}
