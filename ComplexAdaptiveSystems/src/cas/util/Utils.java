package cas.util;

import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Spliterator;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import cas.util.geo.Vec3;
import javafx.beans.InvalidationListener;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.paint.Color;

public final class Utils
{
	public static <T> T any(List<T> c) { return c.get((int) (Math.random() * c.size())); }
	
	public static <S, T> ObservableValue<T> conversionProperty(ObservableValue<S> source, Function<S, T> f)
	{
		SimpleObjectProperty<T> value = new SimpleObjectProperty<>();
		InvalidationListener callback = o -> value.set(f.apply(source.getValue()));
		
		source.addListener(callback);
		
		callback.invalidated(source);
		
		return value;
	}
	
	public static Set<Vec3> equidistantPointsOnSphere(int n)
	{
		Set<Vec3> pts = new HashSet<>();
		
		if(n <= 0)
			throw new IllegalArgumentException();
		
		if(n == 1)
		{
			pts.add(Vec3.Y_AXIS);
			
			return pts;
		}
		
		double a = 4 * Math.PI / n;
		double d = Math.sqrt(a);
		int cx = (int) Math.ceil(Math.PI / d);
		double t = Math.round(a * cx / Math.PI);
		
		for(int ix = 0 ; ix < cx ; ++ix)
		{
			double rx = (ix + 0.5) * Math.PI / cx;
			int cy = (int) Math.ceil(2 * Math.PI * Math.sin(rx) / t);
		
			for(int iy = 0; iy < cy ; ++iy)
			{
				double ry = iy * 2 * Math.PI / cy;
			
				pts.add(Vec3.onSphere(rx, ry));
				
				if(--n == 0)
					return pts;
			}
		}
	
		throw new IllegalStateException("" + n);
	}
	
	private static class BiSpliterator<T, A, B> implements Spliterator<T>
	{
		private final Spliterator<A> ia;
		private final Spliterator<B> ib;
		private final BiFunction<A, B, T> callback;
		
		public BiSpliterator(Spliterator<A> a, Spliterator<B> b, BiFunction<A, B, T> f)
		{
			ia = a;
			ib = b;
			callback = f;
		}

		@Override
		public int characteristics()
		{
			return ia.characteristics() & ib.characteristics();
		}

		@Override
		public long estimateSize()
		{
			return Math.min(ia.estimateSize(), ib.estimateSize());
		}

		@SuppressWarnings("unchecked")
		@Override
		public boolean tryAdvance(Consumer<? super T> f)
		{
			Object[] aa = new Object[1];
			Object[] ab = new Object[1];
			
			if(!ia.tryAdvance(e -> aa[0] = e))
				return false;
			
			if(!ib.tryAdvance(e -> ab[0] = e))
				return false;
			
			f.accept(callback.apply((A) aa[0], (B) ab[0]));
			
			return true;
		}

		@Override
		public Spliterator<T> trySplit()
		{
			return null;
		}
	}
	
	public static <T, A, B> Stream<T> join(Stream<A> a, Stream<B> b, BiFunction<A, B, T> f)
	{
		return StreamSupport.stream(new BiSpliterator<T, A, B>(a.spliterator(), b.spliterator(), f), a.isParallel() && b.isParallel());
	}

	public static String color2s(Color c)
	{
		return String.format("#%02x%02x%02x%02x", (int) (255 * c.getOpacity()), c.getRed(), c.getGreen(), c.getBlue());
	}
	
	public static Color s2color(String s)
	{
		int a = 0, r = 0, g = 0, b = 0;
		
		if(s.length() == 7)
		{
			a = 255;
			s = s.substring(1);
		}
		else if(s.length() == 9)
		{
			a = Integer.parseInt(s.substring(1, 3), 16);
			s = s.substring(3);
		}
		else
		{
			throw new IllegalArgumentException("Malformed color " + s);
		}

		r = Integer.parseInt(s.substring(0, 2), 16);
		g = Integer.parseInt(s.substring(2, 4), 16);
		b = Integer.parseInt(s.substring(4, 6), 16);
		
		return Color.rgb(r, g, b, a / 255.0);
	}
	
	public static final Charset CHARSET = Charset.forName("UTF-8");
	
	private Utils( ) { }
}
