package cas.util.geo;

import dave.json.Container;
import dave.json.JsonObject;
import dave.json.JsonValue;
import dave.json.Loader;
import dave.json.Saveable;
import dave.json.Saver;

@Container
public class Vec3 implements Saveable
{
	public final double X, Y, Z;
	private double mLength;
	
	public Vec3(double x, double y, double z)
	{
		X = x;
		Y = y;
		Z = z;
		mLength = -1;
		
		if(!Double.isFinite(X) || !Double.isFinite(Y) || !Double.isFinite(Z))
			throw new IllegalArgumentException();
	}
	
	@Override
	@Saver
	public JsonValue save( )
	{
		JsonObject json = new JsonObject();

		json.putNumber("x", X);
		json.putNumber("y", Y);
		json.putNumber("z", Z);
		
		return json;
	}
	
	@Loader
	public static Vec3 load(JsonValue json)
	{
		JsonObject o = (JsonObject) json;

		double x = o.getDouble("x");
		double y = o.getDouble("y");
		double z = o.getDouble("z");
		
		return new Vec3(x, y, z);
	}
	
	public double lengthSqr( )
	{
		return X * X + Y * Y + Z * Z;
	}
	
	public double length( )
	{
		if(mLength < 0) mLength = Math.sqrt(lengthSqr());
		
		return mLength;
	}
	
	public Vec3 add(Vec3 v)
	{
		return new Vec3(X + v.X, Y + v.Y, Z + v.Z);
	}
	
	public Vec3 negate( )
	{
		return new Vec3(-X, -Y, -Z);
	}
	
	public Vec3 scale(double f)
	{
		return new Vec3(f * X, f * Y, f * Z);
	}
	
	public Vec3 cross(Vec3 v)
	{
		return new Vec3(
			Y * v.Z - Z * v.Y,
			Z * v.X - X * v.Z,
			X * v.Y - Y * v.X);
	}
	
	public Vec3 normalize( )
	{
		return scale(1 / length());
	}

	public Vec3 lookAt( )
	{
		double yaw = Math.atan2(X, Z);
		double pitch = Math.asin(Y / length());
		
		return new Vec3(yaw, pitch, 0);
	}
	
	public Vec3 lookAt(Vec3 v)
	{
		return v.add(negate()).lookAt();
	}
	
	@Override
	public String toString( )
	{
		return String.format("(%f | %f | %f)", X, Y, Z);
	}
	
	@Override
	public int hashCode( )
	{
		return Double.hashCode(X) * 3 + Double.hashCode(Y) * 13 + Double.hashCode(Z) * 23;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o instanceof Vec3)
		{
			Vec3 v = (Vec3) o;
			
			return X == v.X && Y == v.Y && Z == v.Z;
		}
		
		return false;
	}
	
	public static Vec3 onSphere(double rx, double ry)
	{
		double sin_x = Math.sin(rx), sin_y = Math.sin(ry);
		double cos_x = Math.cos(rx), cos_y = Math.cos(ry);
	
		return new Vec3(sin_x * cos_y, sin_x * sin_y, cos_x);
	}
	
	public static final Vec3 ORIGIN = new Vec3(0, 0, 0);
	public static final Vec3 X_AXIS = new Vec3(1, 0, 0);
	public static final Vec3 Y_AXIS = new Vec3(0, 1, 0);
	public static final Vec3 Z_AXIS = new Vec3(0, 0, 1);
}
