package cas.util.geo;

import javafx.scene.paint.Color;
import javafx.scene.transform.Translate;

public class Point
{
	private final double mX, mY, mZ;
	private final double mValue;
	private final Color mColor;
	
	public Point(double x, double y, double z, double v, Color c)
	{
		mX = x;
		mY = y;
		mZ = z;
		mValue = v;
		mColor = c;
	}
	
	public Vec3 to(Point p)
	{
		return new Vec3(mX - p.mX, mY - p.mY, mZ - p.mZ);
	}
	
	public Translate toTranslation( )
	{
		return new Translate(mX, mY, mZ);
	}
	
	public double getX( ) { return mX; }
	public double getY( ) { return mY; }
	public double getZ( ) { return mZ; }
	public double getValue( ) { return mValue; }
	public Color getColor( ) { return mColor; }
}
