package cas.util.geo;

import javafx.scene.paint.Color;

public class Edge
{
	private final Point mSource, mTarget;
	private final double mValue;
	private final Color mColor;
	
	public Edge(Point s, Point t, double v, Color c)
	{
		mSource = s;
		mTarget = t;
		mValue = v;
		mColor = c;
	}
	
	public Point getSource( ) { return mSource; }
	public Point getTarget( ) { return mTarget; }
	public double getValue( ) { return mValue; }
	public Color getColor( ) { return mColor; }
}
