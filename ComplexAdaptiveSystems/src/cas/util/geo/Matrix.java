package cas.util.geo;

public class Matrix
{
	private final int mWidth, mHeight;
	private final double[] mContent;

	public Matrix(int w, int h) { this(w, h, new double[w * h]); }
	public Matrix(int w, int h, double[] d)
	{
		if(w <= 0 || h <= 0 || d == null || d.length != w * h)
			throw new IllegalArgumentException();
		
		mWidth = w;
		mHeight = h;
		mContent = d;
	}
	
	public int getWidth( ) { return mWidth; }
	public int getHeight( ) { return mHeight; }
	public double get(int x, int y) { return mContent[x + y * mWidth]; }
	public void set(int x, int y, double v) { mContent[x + y * mWidth] = v; }
	public double[] getData( ) { return mContent; }
	
	public Matrix mul(Matrix m)
	{
		if(mWidth != m.mHeight)
			throw new IllegalArgumentException("Mismatched dimensions: " + mWidth + "x" + mHeight + " * " + m.mWidth + "x" + m.mHeight);
		
		Matrix r = new Matrix(m.mWidth, mHeight);
		
		for(int y = 0 ; y < r.mHeight ; ++y)
		{
			for(int x = 0 ; x < r.mWidth ; ++x)
			{
				double v = 0;
				
				for(int i = 0 ; i < mWidth ; ++i)
				{
					v += get(i, y) * m.get(x, i);
				}
				
				r.set(x, y, v);
			}
		}
		
		return r;
	}
	
	public Matrix transpose( )
	{
		Matrix m = new Matrix(mHeight, mWidth);
		
		for(int y = 0 ; y < mHeight ; ++y)
		{
			for(int x = 0 ; x < mWidth ; ++x)
			{
				m.set(y, x, get(x, y));
			}
		}
		
		return m;
	}

	public static Matrix identity(int m)
	{
		Matrix r = new Matrix(m, m);
		
		for(int i = 0 ; i < m ; ++i)
		{
			r.set(i, i, 1);
		}
		
		return r;
	}
	
	public static final double FULL = 2 * Math.PI;
	
	public static double normalize(double a)
	{
		while(a >= FULL)
		{
			a -= FULL;
		}
		
		while(a < 0)
		{
			a += FULL;
		}
		
		return a;
	}
	
	public static Matrix lookAt(Vec3 v)
	{
		if(Math.abs(v.lengthSqr() - 1) > 0.001)
			throw new IllegalArgumentException("LookAt vector must be normalized!");
		
		Vec3 forward = v;
		Vec3 right = Vec3.Y_AXIS.negate().cross(forward);
		Vec3 up = forward.cross(right);
		
		return new Matrix(4, 4, new double[] {
			right.X, right.Y, right.Z, 0,
			up.X, up.Y, up.Z, 0,
			forward.X, forward.Y, forward.Z, 0,
			0, 0, 0, 1
		});
	}
	
	public static Matrix translate(double dx, double dy, double dz)
	{
		return new Matrix(4, 4, new double[] {
				1, 0, 0, dx,
				0, 1, 0, dy,
				0, 0, 1, dz,
				0, 0, 0,  1
		});
	}
	
	public static Matrix rotate(double rx, double ry, double rz)
	{
		return rotate_x(rx).mul(rotate_y(ry)).mul(rotate_z(rz));
	}
	
	public static Matrix rotate_x(double angle)
	{
		angle = normalize(angle);
		
		if(angle == 0)
		{
			return identity(4);
		}
		else
		{
			double sin = Math.sin(angle);
			double cos = Math.cos(angle);
			
			return new Matrix(4, 4, new double[] {
				   1,    0,    0,   0,
				   0,  cos, -sin,   0,
				   0,  sin,  cos,   0,
				   0,    0,    0,   1
			});
		}
	}
	
	public static Matrix rotate_y(double angle)
	{
		angle = normalize(angle);
		
		if(angle == 0)
		{
			return identity(4);
		}
		else
		{
			double sin = Math.sin(angle);
			double cos = Math.cos(angle);
			
			return new Matrix(4, 4, new double[] {
				 cos,    0,  sin,   0,
				   0,    1,    0,   0,
				-sin,    0,  cos,   0,
				   0,    0,    0,   1
			});
		}
	}
	
	public static Matrix rotate_z(double angle)
	{
		angle = normalize(angle);
		
		if(angle == 0)
		{
			return identity(4);
		}
		else
		{
			double sin = Math.sin(angle);
			double cos = Math.cos(angle);
			
			return new Matrix(4, 4, new double[] {
				 cos, -sin,    0,   0,
				 sin,  cos,    0,   0,
				   0,    0,    1,   0,
				   0,    0,    0,   1
			});
		}
	}
}
