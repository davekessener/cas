package cas.util.geo.fx;

import cas.util.geo.Point;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Sphere;

public class CASSphere extends Sphere
{
	public CASSphere(Point p)
	{
		super(p.getValue());
		
		PhongMaterial m = new PhongMaterial();
		
		m.setDiffuseColor(p.getColor());
		
		this.getTransforms().add(p.toTranslation());
		this.setMaterial(m);
	}
}
