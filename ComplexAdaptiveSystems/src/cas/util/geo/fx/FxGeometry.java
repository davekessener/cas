package cas.util.geo.fx;

import cas.util.geo.Matrix;
import cas.util.geo.Vec3;

public class FxGeometry extends FxTransform
{
	private double mPitch = 0, mYaw = 0, mRoll = 0;
	private double mX = 0, mY = 0, mZ = 0;
	
	public void moveTo(Vec3 p)
	{
		mX = p.X;
		mY = p.Y;
		mZ = p.Z;
		
		update();
	}
	
	public Vec3 getPosition( ) { return new Vec3(mX, -mY, mZ); }
	public Vec3 getRotation( ) { return new Vec3(mPitch, mYaw, mRoll); }
	
	protected void doSetPosition(double x, double y, double z) { mX = x; mY = y; mZ = z; }
	protected void doSetRotation(double rx, double ry, double rz) { mPitch = rx; mYaw = ry; mRoll = rz; }
	
	public void move(double dx, double dy, double dz)
	{
		if(dx != 0 || dy != 0 || dz != 0)
		{
			Matrix m = new Matrix(4, 4, new double[] {
				1, 0, 0, dx,
				0, 1, 0, dy,
				0, 0, 1, dz,
				0, 0, 0,  1
			});
			
			m = rotationMatrix().mul(m);
	
			mX += m.get(3, 0);
			mY += m.get(3, 1);
			mZ += m.get(3, 2);
			
			update();
		}
	}
	
	public void rotate(double rx, double ry, double rz)
	{
		rotate(new Vec3(rx, ry, rz));
	}
	
	public void rotate(Vec3 r)
	{
		setRotation(getRotation().add(r));
	}
	
	public void setRotation(Vec3 r)
	{
		if(r.X != mPitch || r.Y != mYaw || r.Z != mRoll)
		{
			mPitch = r.X;
			mYaw = r.Y;
			mRoll = r.Z;
			
			update();
		}
	}
	
	protected void update( )
	{
		set(translationMatrix().mul(rotationMatrix()));
	}
	
	public Matrix translationMatrix( )
	{
		return new Matrix(4, 4, new double[] {
			1, 0, 0, mX,
			0, 1, 0, mY,
			0, 0, 1, mZ,
			0, 0, 0, 1
		});
	}
	
	public Matrix rotationMatrix( )
	{
		return Matrix.rotate_y(mPitch).mul(Matrix.rotate_x(mYaw));
	}
}
