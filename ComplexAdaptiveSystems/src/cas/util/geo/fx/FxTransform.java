package cas.util.geo.fx;

import cas.util.geo.Matrix;
import javafx.scene.transform.Affine;
import javafx.scene.transform.MatrixType;
import javafx.scene.transform.Transform;

public class FxTransform
{
	private final Affine mTransform = new Affine();
	
	public Transform getTransform( ) { return mTransform; }
	
	public void prepend(Matrix m)
	{
		check(m);
		
		mTransform.prepend(m.getData(), MatrixType.MT_3D_4x4, 0);
	}
	
	public void append(Matrix m)
	{
		check(m);
		
		mTransform.append(m.getData(), MatrixType.MT_3D_4x4, 0);
	}
	
	public void set(Matrix m)
	{
		check(m);
		
		mTransform.setToTransform(m.getData(), MatrixType.MT_3D_4x4, 0);
	}
	
	private void check(Matrix m)
	{
		if(m.getWidth() != 4 || m.getHeight() != 4)
			throw new IllegalArgumentException("Unexpected size " + m.getWidth() + "x" + m.getHeight());
	}
}
