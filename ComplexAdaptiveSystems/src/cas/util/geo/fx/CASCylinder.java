package cas.util.geo.fx;

import cas.util.geo.Edge;
import cas.util.geo.Vec3;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Cylinder;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Translate;

public class CASCylinder extends Cylinder
{
	public CASCylinder(Edge e)
	{
		super(0, 0, DIVISIONS);
		
		Vec3 v = e.getSource().to(e.getTarget());
		Vec3 r = v.lookAt();
		PhongMaterial m = new PhongMaterial();
		
		m.setDiffuseColor(e.getColor());
		
		this.setRadius(e.getValue());
		this.setHeight(v.length());
		this.setMaterial(m);
		
		this.getTransforms().addAll(
			e.getSource().toTranslation(),
			new Rotate(r.Y * 180 / Math.PI, Rotate.Y_AXIS),
			new Rotate(90 + r.X * 180 / Math.PI, Rotate.X_AXIS),
			new Translate(0, -this.getHeight() / 2, 0)
		);
	}
	
	private static final int DIVISIONS = 5;
}
