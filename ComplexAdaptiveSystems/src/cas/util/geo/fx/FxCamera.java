package cas.util.geo.fx;

import cas.util.geo.Vec3;

public class FxCamera extends FxGeometry
{
	public void lookAt(Vec3 p)
	{
		Vec3 v = getPosition().lookAt(p);
		
		setRotation(v);
		
		update();
	}
	
	@Override
	protected void update( )
	{
		Vec3 r = getRotation();

		if(r.Y < -FOV) doSetRotation(r.X, -FOV, r.Z);
		if(r.Y >  FOV) doSetRotation(r.X,  FOV, r.Z);
		
		super.update();
	}

	private static final double FOV = Math.PI / 2;
}
