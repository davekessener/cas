package cas.util.geo;

public interface Geometry
{
	public abstract void addPoint(Point p);
	public abstract void addEdge(Edge e);
}
