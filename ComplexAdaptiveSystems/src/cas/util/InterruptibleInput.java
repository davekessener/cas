package cas.util;

import java.io.IOException;
import java.io.Reader;

public class InterruptibleInput implements AutoCloseable
{
	private final Reader mIn;
	private boolean mInterrupted;
	
	public InterruptibleInput(Reader in)
	{
		mIn = in;
		mInterrupted = false;
	}
	
	public void interrupt( )
	{
		mInterrupted = true;
	}
	
	public String nextLine( ) throws InterruptedException, IOException
	{
		StringBuilder sb = new StringBuilder();
		
		while(true)
		{
			if(mInterrupted)
				throw new InterruptedException();
			
			if(mIn.ready())
			{
				char c = (char) mIn.read();
				
				if(c == '\n') break;
				
				if(c != '\r')
				{
					sb.append(c);
				}
			}
			else
			{
				Thread.sleep(100);
			}
		}
		
		return sb.toString();
	}

	@Override
	public void close()
	{
		interrupt();
		
		try
		{
			mIn.close();
		}
		catch (IOException e)
		{
			throw new SevereException(e);
		}
	}
}
